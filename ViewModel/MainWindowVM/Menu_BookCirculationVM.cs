﻿using ALIS_Proj.Model;
using ALIS_Proj.Model.Entities;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace ALIS_Proj.ViewModel.MainWindowVM
{
    public class Menu_BookCirculationVM : ViewModelBase
    {
        public ObservableCollection<Book> giveBooksByBarcodeList;
        public ObservableCollection<Book> booksOfCurrentPersonList;
        private ICollectionView _catalogOfBooksCollection;
        private ICollectionView _booksOfCurrentPersonCollection;

        public Person currentPerson = new Person();

        private Book _giveBookByBarcodeSelected;
        private Book _booksOfCurrentPersonSelected;
        private string _activePersonBarcodeFromForm;
        private string _bookBarcodeFromForm;
        private BitmapImage _barcodeImage;

        private RelayCommand _buttonGiveBookByBarcodePreload;
        private RelayCommand _buttonGiveBookByBarcodeCancel;
        private RelayCommand _buttonGiveBookByBarcodeDelete;
        private RelayCommand _buttonGiveBookByBarcodeOk;

        public delegate void DoSomething();
        public event DoSomething NotifyGiveBookByBarcodePreload;
        public event DoSomething NotifyGiveBookByBarcodeCloseWindow;

        public ICollectionView BooksOfCurrentPersonCollection
        {
            get => _booksOfCurrentPersonCollection;
            set
            {
                _booksOfCurrentPersonCollection = value;
                OnPropertyChanged(nameof(BooksOfCurrentPersonCollection));
            }
        }

        public ICollectionView CatalogOfBooksCollection
        {
            get => _catalogOfBooksCollection;
            set
            {
                _catalogOfBooksCollection = value;
                OnPropertyChanged(nameof(CatalogOfBooksCollection));
            }
        }

        public Book GiveBookByBarcodeSelected
        {
            get
            {
                return _giveBookByBarcodeSelected;
            }
            set
            {
                //Игнорируем повторный выбор пользователя
                if (_giveBookByBarcodeSelected == value)
                    return;
                _giveBookByBarcodeSelected = value;
                OnPropertyChanged(nameof(GiveBookByBarcodeSelected));
            }
        }

        public Book BooksOfCurrentPersonSelected
        {
            get
            {
                return _booksOfCurrentPersonSelected;
            }
            set
            {
                //Игнорируем повторный выбор пользователя
                if (_booksOfCurrentPersonSelected == value)
                    return;
                _booksOfCurrentPersonSelected = value;
                OnPropertyChanged(nameof(BooksOfCurrentPersonSelected));
            }
        }

        public string ActivePersonBarcodeFromForm
        {
            get
            {
                return _activePersonBarcodeFromForm;
            }
            set
            {
                if (value.Length == 8)
                {
                    Person temp = Staff.GetOnePersonByBarcode(value);
                    if (!String.IsNullOrWhiteSpace(temp.Name) )
                    {
                        currentPerson = temp;
                        BarcodeImage = Staff.GetBarcodeImage(currentPerson);
                        booksOfCurrentPersonList = BooksCirculation.GetBooksOfPersonFromDB(currentPerson);
                        BooksOfCurrentPersonCollection = CollectionViewSource.GetDefaultView(booksOfCurrentPersonList);
                        BooksOfCurrentPersonCollection.SortDescriptions.Add(new SortDescription("ISBN", ListSortDirection.Ascending));
                        BooksOfCurrentPersonCollection.MoveCurrentToFirst();
                        GiveBookByBarcodeSelected = (Book)BooksOfCurrentPersonCollection.CurrentItem;
                    }
                }
                _activePersonBarcodeFromForm = value;
            }
        }

        public string BookBarcodeFromForm
        {
            get 
            { 
                return _bookBarcodeFromForm;
            }
            set 
            {
                if (value.Length == 8)
                {
                    Book temp = Books.GetOneBookFromDB(value);
                    if (!String.IsNullOrWhiteSpace(temp.Name)
                        && !giveBooksByBarcodeList.Any(x => x.Barcode == value))
                    {
                        int idOvnerPerson = Books.IsBookAvailable(temp);
                        if (idOvnerPerson == -1)
                        {
                            giveBooksByBarcodeList.Add(temp);
                            CatalogOfBooksCollection = CollectionViewSource.GetDefaultView(giveBooksByBarcodeList);
                            CatalogOfBooksCollection.SortDescriptions.Add(new SortDescription("ISBN", ListSortDirection.Ascending));
                            CatalogOfBooksCollection.MoveCurrentToFirst();
                            GiveBookByBarcodeSelected = (Book)CatalogOfBooksCollection.CurrentItem;
                        }
                        else
                        {
                            MessageBox.Show($"Книга уже выдана читателю {Staff.GetOnePersonById(idOvnerPerson).FullName}","Данная книга занята");
                        }
                    }
                }
                _bookBarcodeFromForm = value; 
            }
        }


        public BitmapImage BarcodeImage 
        {
            get
            {
                return _barcodeImage;
            }
            set
            {
                _barcodeImage = value;
                OnPropertyChanged(nameof(BarcodeImage));
            }
        }

        public RelayCommand ButtonGiveBookByBarcodePreload
        {
            get
            {
                return _buttonGiveBookByBarcodePreload ??
                       (_buttonGiveBookByBarcodePreload = new RelayCommand(obj =>
                       {
                           NotifyGiveBookByBarcodePreload?.Invoke();
                       }, obj =>
                       {
                           //Кнопка всегда доступна если выбран пользователь (по штрихкоду)
                           return !String.IsNullOrWhiteSpace(currentPerson.Name);
                       }));
            }
        }

        public RelayCommand ButtonGiveBookByBarcodeCancel
        {
            get
            {
                return _buttonGiveBookByBarcodeCancel ??
                       (_buttonGiveBookByBarcodeCancel = new RelayCommand(obj =>
                       {
                           giveBooksByBarcodeList.Clear();
                           NotifyGiveBookByBarcodeCloseWindow?.Invoke();
                       }, obj =>
                       {
                           //Кнопка всегда доступна если выбран пользователь (по штрихкоду)
                           return true;
                       }));
            }
        }

        public RelayCommand ButtonGiveBookByBarcodeDelete
        {
            get
            {
                return _buttonGiveBookByBarcodeDelete ??
                       (_buttonGiveBookByBarcodeDelete = new RelayCommand(obj =>
                       {
                           giveBooksByBarcodeList.Remove(GiveBookByBarcodeSelected);
                           CatalogOfBooksCollection = CollectionViewSource.GetDefaultView(giveBooksByBarcodeList);
                           CatalogOfBooksCollection.SortDescriptions.Add(new SortDescription("ISBN", ListSortDirection.Ascending));
                           CatalogOfBooksCollection.MoveCurrentToFirst();
                           GiveBookByBarcodeSelected = (Book)CatalogOfBooksCollection.CurrentItem;
                       }, obj =>
                       {
                           //Кнопка всегда доступна если выбран пользователь (по штрихкоду)
                           return true;
                       }));
            }
        }

        public RelayCommand ButtonGiveBookByBarcodeOk
        {
            get
            {
                return _buttonGiveBookByBarcodeOk ??
                       (_buttonGiveBookByBarcodeOk = new RelayCommand(obj =>
                       {
                           foreach (var item in giveBooksByBarcodeList)
                           {
                               BooksCirculation.SetBookToPerson(item, currentPerson);

                               booksOfCurrentPersonList = BooksCirculation.GetBooksOfPersonFromDB(currentPerson);
                               BooksOfCurrentPersonCollection = CollectionViewSource.GetDefaultView(booksOfCurrentPersonList);
                               BooksOfCurrentPersonCollection.SortDescriptions.Add(new SortDescription("ISBN", ListSortDirection.Ascending));
                               BooksOfCurrentPersonCollection.MoveCurrentToFirst();
                               GiveBookByBarcodeSelected = (Book)BooksOfCurrentPersonCollection.CurrentItem;

                               NotifyGiveBookByBarcodeCloseWindow?.Invoke();
                           }


                       }, obj =>
                       {
                           //Кнопка всегда доступна если выбран пользователь (по штрихкоду)
                           return true;
                       }));
            }
        }

        public Menu_BookCirculationVM()
        {
            giveBooksByBarcodeList = new ObservableCollection<Book>();
        }
    }
}

﻿using ALIS_Proj.Model;
using ALIS_Proj.Model.Entities;
using ALIS_Proj.Model.Enums;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace ALIS_Proj.ViewModel.MainWindowVM
{
    public class Menu_UserAccountingVM : ViewModelBase
    {
        public ObservableCollection<Person> _persons;
        public Person _selectedPerson;
        private ICollectionView _personsCollection;
        private String _columnSurnameFilterString = String.Empty;
        private String _columnNameFilterString = String.Empty;
        private String _columnPatronymicFilterString = String.Empty;
        private String _columnAccessRoleFilterString = String.Empty;
        private String _columnCreateDateFilterString = String.Empty;
        private String _columnBirthdayFilterString = String.Empty;
        private String _columnPhoneNumberFilterString = String.Empty;
        private String _columnAltPhoneNumberFilterString = String.Empty;
        private String _columnHomeAddressFilterString = String.Empty;
        private String _columnSchoolGroupNumberFilterString = String.Empty;
        private String _columnPasportDataFilterString = String.Empty;
        private String _columnBarcodeFilterString = String.Empty;

        private RelayCommand _buttonCreatePersonPreload;
        private RelayCommand _buttonCreatePerson;
        private RelayCommand _buttonEditPersonPreload;
        private RelayCommand _buttonEditPerson;
        private RelayCommand _buttonDeletePerson;
        private RelayCommand _buttonBarcodeCreatePreload;
        private RelayCommand _buttonBarcodeSaveToFile;
        private RelayCommand _buttonBarcodeSendToPrint;
        private RelayCommand _buttonFindPersonPreload;
        private RelayCommand _buttonFindPersonCancel;
        private RelayCommand _buttonFindPersonOk;
        private RelayCommand _buttonClearFilter;

        public delegate void DoSomething();
        public event DoSomething NotifyCreatePerson;
        public event DoSomething NotifyCreatePersonPreload;
        public event DoSomething NotifyEditPerson;
        public event DoSomething NotifyEditPersonPreload;
        public event DoSomething NotifyBarcodeCreatePreload;
        public event DoSomething NotifyFindPersonPreload;
        public event DoSomething NotifyFindPersonClose;

        /// <summary>
        /// Пользователь на добавление
        /// </summary>
        public Person NewPerson { get; set; } = new Person();
        public Person EditPerson { get; set; } = new Person();
        public BitmapImage BarcodeImage { get; set; } = new BitmapImage();

        #region ОПИСАНИЕ СВОЙСТВ

        /// <summary>
        /// Коллекция пользователей. Содержит полный перечень пользователей и их свойства.
        /// </summary>
        public ObservableCollection<Person> Persons
        {
            get => _persons;
            set
            {
                _persons = value;
            }
        }

        /// <summary>
        /// Текущий выбранный пользователь
        /// </summary>
        public Person SelectedPerson
        {
            get => _selectedPerson;
            set
            {
                //Игнорируем повторный выбор пользователя
                if (_selectedPerson == value)
                    return;
                _selectedPerson = value;
                OnPropertyChanged(nameof(SelectedPerson));
            }
        }

        /// <summary>
        /// Интерфейс для реализации коллекции
        /// </summary>
        public ICollectionView PersonsCollection
        {
            get => _personsCollection;
            set
            {
                _personsCollection = value;
                OnPropertyChanged(nameof(PersonsCollection));
            }
        }

        #region СВОЙСТВО ФИЛЬТРА
        public String ColumnSurnameFilterString
        {
            get => _columnSurnameFilterString;
            set
            {
                _columnSurnameFilterString = value;
                OnPropertyChanged(nameof(ColumnSurnameFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnNameFilterString
        {
            get => _columnNameFilterString;
            set
            {
                _columnNameFilterString = value;
                OnPropertyChanged(nameof(ColumnNameFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnPatronymicFilterString
        {
            get => _columnPatronymicFilterString;
            set
            {
                _columnPatronymicFilterString = value;
                OnPropertyChanged(nameof(ColumnPatronymicFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnAccessRoleFilterString
        {
            get => _columnAccessRoleFilterString;
            set
            {
                if (value == "0") _columnAccessRoleFilterString = AccessRole.Client.ToString();
                else if (value == "1") _columnAccessRoleFilterString = AccessRole.Admin.ToString();
                else _columnAccessRoleFilterString = value;

                OnPropertyChanged(nameof(ColumnAccessRoleFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnCreateDateFilterString
        {
            get => _columnCreateDateFilterString;
            set
            {
                _columnCreateDateFilterString = value;
                OnPropertyChanged(nameof(ColumnCreateDateFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnBirthdayFilterString
        {
            get => _columnBirthdayFilterString;
            set
            {
                _columnBirthdayFilterString = value;
                OnPropertyChanged(nameof(ColumnBirthdayFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnPhoneNumberFilterString
        {
            get => _columnPhoneNumberFilterString;
            set
            {
                _columnPhoneNumberFilterString = value;
                OnPropertyChanged(nameof(ColumnPhoneNumberFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnAltPhoneNumberFilterString
        {
            get => _columnAltPhoneNumberFilterString;
            set
            {
                _columnAltPhoneNumberFilterString = value;
                OnPropertyChanged(nameof(ColumnAltPhoneNumberFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnHomeAddressFilterString
        {
            get => _columnHomeAddressFilterString;
            set
            {
                _columnHomeAddressFilterString = value;
                OnPropertyChanged(nameof(ColumnHomeAddressFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnSchoolGroupNumberFilterString
        {
            get => _columnSchoolGroupNumberFilterString;
            set
            {
                _columnSchoolGroupNumberFilterString = value;
                OnPropertyChanged(nameof(ColumnSchoolGroupNumberFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnPasportDataFilterString
        {
            get => _columnPasportDataFilterString;
            set
            {
                _columnPasportDataFilterString = value;
                OnPropertyChanged(nameof(ColumnPasportDataFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        public String ColumnBarcodeFilterString
        {
            get => _columnBarcodeFilterString;
            set
            {
                _columnBarcodeFilterString = value;
                OnPropertyChanged(nameof(ColumnBarcodeFilterString));
                PersonsCollection.Filter += Filter;
            }
        }
        #endregion

        #endregion

        #region ОБСЛУЖИВАЮЩИЕ МЕТОДЫ
        /// <summary>
        /// Предикат фильтрации коллекции книг
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        private bool Filter(object emp)
        {
            Person person = emp as Person;

            return (person.Surname.ToLower().Contains(ColumnSurnameFilterString.ToLower())
                && person.Name.ToLower().Contains(ColumnNameFilterString.ToLower())
                && person.Patronymic.ToLower().Contains(ColumnPatronymicFilterString.ToLower())
                && person.AccessRole.ToString().ToLower().Contains(ColumnAccessRoleFilterString.ToLower())
                && person.CreateDate.ToString().ToLower().Contains(ColumnCreateDateFilterString.ToLower())
                && person.Birthday.ToString().ToLower().Contains(ColumnBirthdayFilterString.ToLower())
                && person.PhoneNumber.ToLower().Contains(ColumnPhoneNumberFilterString.ToLower())
                && person.AltPhoneNumber.ToLower().Contains(ColumnAltPhoneNumberFilterString.ToLower())
                && person.HomeAddress.ToLower().Contains(ColumnHomeAddressFilterString.ToLower())
                && person.SchoolGroupNumber.ToString().ToLower().Contains(ColumnSchoolGroupNumberFilterString.ToLower())
                && person.PasportData.ToString().ToLower().Contains(ColumnPasportDataFilterString.ToLower())
                && person.Barcode.ToLower().Contains(ColumnBarcodeFilterString.ToLower())
                );
        }

        private void ClearFilter()
        {
            ColumnSurnameFilterString = String.Empty;
            ColumnNameFilterString = String.Empty;
            ColumnPatronymicFilterString = String.Empty;
            ColumnAccessRoleFilterString = String.Empty;
            ColumnCreateDateFilterString = String.Empty;
            ColumnBirthdayFilterString = String.Empty;
            ColumnPhoneNumberFilterString = String.Empty;
            ColumnAltPhoneNumberFilterString = String.Empty;
            ColumnHomeAddressFilterString = String.Empty;
            ColumnSchoolGroupNumberFilterString = String.Empty;
            ColumnPasportDataFilterString = String.Empty;
            ColumnBarcodeFilterString = String.Empty;
        }
        #endregion

        #region ОБРАБОТКА КНОПОК
        /// <summary>
        /// Кнопка добавления пользователя (выполняется при открытии окна добавления пользователя)
        /// </summary>
        public RelayCommand ButtonCreatePersonPreload
        {
            get
            {
                return _buttonCreatePersonPreload ??
                       (_buttonCreatePersonPreload = new RelayCommand(obj =>
                       {
                           //Обнуляем новую книгу 
                           NewPerson = new Person();

                           //Сообщаем GUI о необходимости открыть окно добавления пользователей
                           NotifyCreatePersonPreload?.Invoke();
                       }, obj =>
                       {
                           //Кнопка доступна всегда
                           return true;
                       }));
            }
        }
        /// <summary>
        /// Кнопка добавления пользователя (принятие изменений в окне добавления пользователя)
        /// </summary>
        public RelayCommand ButtonCreatePerson
        {
            get
            {
                return _buttonCreatePerson ??
                       (_buttonCreatePerson = new RelayCommand(obj =>
                       {
                           //Добавляем нового пользователя в базу
                           Staff.AddNewPersonToDB(NewPerson);

                           //Обновляем представление пользователей из базы
                           Persons = Staff.GetPersonsFromDB(Model.Enums.AccessRole.All);
                           if (Persons == null)
                           {
                               MessageBox.Show(
                                   "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                                   "Ошибка подключения к БД");
                           }
                           else if (Persons.Any())
                           {
                               //Если каталог из базы загружен успешно, устанавливаем добавленную книгу текущей
                               SelectedPerson = Persons.FirstOrDefault(x => x.Name == NewPerson.Name);
                               //Обновляем коллекцию книг, устанавливаем сортировку
                               PersonsCollection = CollectionViewSource.GetDefaultView(Persons);
                               PersonsCollection.SortDescriptions.Add(new SortDescription("Surname", ListSortDirection.Ascending));
                               //Устанавливаем выбранной книгой первую в отсортированной коллекции
                               PersonsCollection.MoveCurrentTo(Persons.FirstOrDefault(x => x.Name == NewPerson.Name));
                               SelectedPerson = (Person)PersonsCollection.CurrentItem;
                           }

                           //Закрываем окно
                           NotifyCreatePerson?.Invoke();
                           
                       }, obj =>
                       {
                           //Кнопка доступна только если заполнено поле "Название книги"
                           return !(String.IsNullOrWhiteSpace(NewPerson.Name) == true);
                       }));
            }
        }

        /// <summary>
        /// Кнопка редактирования пользователя (выполняется при открытии окна редактирования пользователя)
        /// </summary>
        public RelayCommand ButtonEditPersonPreload
        {
            get
            {
                return _buttonEditPersonPreload ??
                       (_buttonEditPersonPreload = new RelayCommand(obj =>
                       {
                           //Обнуляем новую книгу 
                           EditPerson = (Person)SelectedPerson.Clone();

                           //Сообщаем GUI о необходимости открыть окно добавления пользователей
                           NotifyEditPersonPreload?.Invoke();
                       }, obj =>
                       {
                           //Кнопка недоступна, если не выбран ни один пользователь
                           return (!(SelectedPerson == null));
                       }));
            }
        }

        /// <summary>
        /// Кнопка редактирования пользователя (принятие изменений в окне редактирования пользователя)
        /// </summary>
        public RelayCommand ButtonEditPerson
        {
            get
            {
                return _buttonEditPerson ??
                       (_buttonEditPerson = new RelayCommand(obj =>
                       {
                           //Добавляем нового пользователя в базу
                           Staff.UpdateNewPersonToDB(EditPerson);

                           //Обновляем представление пользователей из базы
                           Persons = Staff.GetPersonsFromDB(Model.Enums.AccessRole.All);
                           if (Persons == null)
                           {
                               MessageBox.Show(
                                   "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                                   "Ошибка подключения к БД");
                           }
                           else if (Persons.Any())
                           {
                               //Если каталог из базы загружен успешно, устанавливаем добавленную книгу текущей
                               SelectedPerson = Persons.FirstOrDefault(x => x.Id == EditPerson.Id);
                               //Обновляем коллекцию книг, устанавливаем сортировку
                               PersonsCollection = CollectionViewSource.GetDefaultView(Persons);
                               PersonsCollection.SortDescriptions.Add(new SortDescription("Surname", ListSortDirection.Ascending));
                               //Устанавливаем выбранной книгой первую в отсортированной коллекции
                               PersonsCollection.MoveCurrentTo(Persons.FirstOrDefault(x => x.Id == EditPerson.Id));
                               SelectedPerson = (Person)PersonsCollection.CurrentItem;
                           }

                           //Закрываем окно
                           NotifyEditPerson?.Invoke();

                       }, obj =>
                       {
                           //Кнопка доступна только если заполнены поля имени
                           return !EditPerson.NamesIsEmpty();
                       }));
            }
        }

        /// <summary>
        /// Кнопка удаления пользователя
        /// </summary>
        public RelayCommand ButtonDeletePerson
        {
            get
            {
                return _buttonDeletePerson ??
                       (_buttonDeletePerson = new RelayCommand(obj =>
                       {
                           var result = MessageBox.Show(
                               $"Вы уверены, что хотите удалить читателя '{SelectedPerson.FullName}'?",
                               "Подтвердите удаление читателя",
                                 MessageBoxButton.YesNo,
                                 MessageBoxImage.Question);

                           // If the no button was pressed ...
                           if (result == MessageBoxResult.Yes)
                           {
                               //Если получено подтверждение удаления книги, удаляем книгу из базы
                               Staff.DeletePersonDB(SelectedPerson);

                               //Обновляем представление пользователей из базы
                               Persons = Staff.GetPersonsFromDB(Model.Enums.AccessRole.All);
                               if (Persons == null)
                               {
                                   MessageBox.Show(
                                       "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                                       "Ошибка подключения к БД");
                               }
                               else if (Persons.Any())
                               {
                                   //Если каталог из базы загружен успешно, устанавливаем добавленную книгу текущей
                                   SelectedPerson = Persons.FirstOrDefault(x => x.Name == NewPerson.Name);
                                   //Обновляем коллекцию книг, устанавливаем сортировку
                                   PersonsCollection = CollectionViewSource.GetDefaultView(Persons);
                                   PersonsCollection.SortDescriptions.Add(new SortDescription("Surname", ListSortDirection.Ascending));
                                   //Устанавливаем выбранной книгой первую в отсортированной коллекции
                                   PersonsCollection.MoveCurrentTo(Persons.FirstOrDefault(x => x.Name == NewPerson.Name));
                                   SelectedPerson = (Person)PersonsCollection.CurrentItem;
                               }
                           }
                       }, obj =>
                       {
                           //Кнопка недоступна, если не выбран ни один пользователь
                           return (!(SelectedPerson == null));
                       }));
            }
        }

        /// <summary>
        /// Кнопка создания читательского билета со штрихкодом (выполняется при открытии окна создания чит. билета пользователя)
        /// </summary>
        public RelayCommand ButtonBarcodeCreatePreload
        {
            get
            {
                return _buttonBarcodeCreatePreload ??
                       (_buttonBarcodeCreatePreload = new RelayCommand(obj =>
                       {
                           //Генерируем читательский билет со штрихкодом
                           BarcodeImage = Staff.GetBarcodeImage(SelectedPerson);

                           //Сообщаем GUI о необходимости открыть окно добавления пользователей
                           NotifyBarcodeCreatePreload?.Invoke();
                       }, obj =>
                       {
                           //Кнопка недоступна, если не выбран ни один пользователь
                           return (!(SelectedPerson == null));
                       }));
            }
        }

        /// <summary>
        /// Кнопка сохранения в файл читательского билета со штрихкодом
        /// </summary>
        public RelayCommand ButtonBarcodeSaveToFile
        {
            get
            {
                return _buttonBarcodeSaveToFile ??
                       (_buttonBarcodeSaveToFile = new RelayCommand(obj =>
                       {
                           Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                           dlg.FileName = "barcode";
                           dlg.DefaultExt = ".png";
                           dlg.Filter = "Png Image|*.png";

                           Nullable<bool> result = dlg.ShowDialog();
                           if (result == true)
                           {
                               var bitmapTemp = new WriteableBitmap((BitmapSource)BarcodeImage);
                               var encoder = new PngBitmapEncoder(); // Or PngBitmapEncoder, or whichever encoder you want
                               encoder.Frames.Add(BitmapFrame.Create(bitmapTemp));
                               using (var stream = dlg.OpenFile())
                               {
                                   encoder.Save(stream);
                               }
                           }
                       }, obj =>
                       {
                           //Кнопка всегда доступна
                           return true;
                       }));
            }
        }

        /// <summary>
        /// Кнопка сохранения в файл читательского билета со штрихкодом
        /// </summary>
        public RelayCommand ButtonBarcodeSendToPrint
        {
            get
            {
                return _buttonBarcodeSendToPrint ??
                       (_buttonBarcodeSendToPrint = new RelayCommand(obj =>
                       {
                           System.Windows.Forms.PrintDialog printDialog1 = new System.Windows.Forms.PrintDialog();
                           System.Drawing.Printing.PrintDocument Document = new System.Drawing.Printing.PrintDocument();
                           Document.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(Document_PrintPage);
                           System.Windows.Forms.DialogResult result = printDialog1.ShowDialog();
                           if (result == System.Windows.Forms.DialogResult.OK)
                           {
                               Document.Print();
                           }

                           void Document_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
                           {
                               Bitmap bmpOut = null;

                               using (MemoryStream ms = new MemoryStream())
                               {
                                   PngBitmapEncoder encoder = new PngBitmapEncoder();
                                   encoder.Frames.Add(BitmapFrame.Create((BitmapSource)BarcodeImage));
                                   encoder.Save(ms);

                                   using (Bitmap bmp = new Bitmap(ms))
                                   {
                                       bmpOut = new Bitmap(bmp);
                                   }
                               }

                               e.Graphics.DrawImage(bmpOut, new System.Drawing.Point(0, 0)); //Картинка на печать
                           }

                       }, obj =>
                       {
                           //Кнопка всегда доступна
                           return true;
                       }));
            }
        }

        /// <summary>
        /// Кнопка создания поиска читателя (выполняется при открытии окна поиска читателя)
        /// </summary>
        public RelayCommand ButtonFindPersonPreload
        {
            get
            {
                return _buttonFindPersonPreload ??
                       (_buttonFindPersonPreload = new RelayCommand(obj =>
                       {
                           //Сообщаем GUI о необходимости открыть окно поиска читателей
                           NotifyFindPersonPreload?.Invoke();
                       }, obj =>
                       {
                           //Кнопка всегда доступна
                           return true;
                       }));
            }
        }

        /// <summary>
        /// Кнопка закрытия окна поиска читателя
        /// </summary>
        public RelayCommand ButtonFindPersonCancel
        {
            get
            {
                return _buttonFindPersonCancel ??
                       (_buttonFindPersonCancel = new RelayCommand(obj =>
                       {
                           //Очищаем фильтры поиска
                           ClearFilter();
                           //Сообщаем GUI о необходимости закрыть окно поиска читателей
                           NotifyFindPersonClose?.Invoke();
                       }, obj =>
                       {
                           //Кнопка всегда доступна
                           return true;
                       }));
            }
        }

        /// <summary>
        /// Кнопка ОК в окне поиска читателя
        /// </summary>
        public RelayCommand ButtonFindPersonOk
        {
            get
            {
                return _buttonFindPersonOk ??
                       (_buttonFindPersonOk = new RelayCommand(obj =>
                       {
                           //Сообщаем GUI о необходимости закрыть окно поиска читателей
                           NotifyFindPersonClose?.Invoke();
                       }, obj =>
                       {
                           //Кнопка всегда доступна
                           return true;
                       }));
            }
        }

        /// <summary>
        /// Кнопка очистки фильтра
        /// </summary>
        public RelayCommand ButtonClearFilter
        {
            get
            {
                return _buttonClearFilter ??
                       (_buttonClearFilter = new RelayCommand(obj =>
                       {
                           //Очищаем фильтры поиска
                           ClearFilter();
                       }, obj =>
                       {
                           //Кнопка всегда доступна
                           return true;
                       }));
            }
        }
        #endregion

        #region КОНСТРУКТОР
        public Menu_UserAccountingVM()
        {
            //Обновляем представление пользователей из базы
            Persons = Staff.GetPersonsFromDB(Model.Enums.AccessRole.All);

            if (Persons == null)
            {
                MessageBox.Show(
                    "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                    "Ошибка подключения к БД");
            }
            else if (Persons.Any())
            {
                //Если каталог из базы загружен успешно, обновляем и сортируем коллекцию книг, устанавливаем выбранной книгой первую
                SelectedPerson = Persons.First();
                PersonsCollection = CollectionViewSource.GetDefaultView(Persons);
                PersonsCollection.SortDescriptions.Add(new SortDescription("Surname", ListSortDirection.Ascending));
                PersonsCollection.MoveCurrentToFirst();
                SelectedPerson = (Person)PersonsCollection.CurrentItem;
            }
        }
        #endregion
    }
}

﻿using ALIS_Proj.Model;
using ALIS_Proj.Model.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace ALIS_Proj.ViewModel.MainWindowVM
{
    public class Menu_CatalogVM : ViewModelBase
    {
        BitmapImage _barcodeImage = new BitmapImage();
        public ObservableCollection<Book> _catalogOfBooks;
        public Book _selectedBook;
        private ICollectionView _catalogOfBooksCollection;
        private int _isObligatoryCopy_RadioButton;
        private String _columnNameFilterString = String.Empty;
        private String _columnMainAuthorFilterString = String.Empty;
        private String _columnNumberOfBooksFilterString = String.Empty;
        private String _columnOtherAuthorsFilterString = String.Empty;
        private String _columnDescriptionFilterString = String.Empty;
        private String _columnTagsFilterString = String.Empty;
        private String _columnNumberOfPagesFilterString = String.Empty;
        private String _columnISBNFilterString = String.Empty;
        private String _columnPublisherFilterString = String.Empty;
        private String _columnPublicationYearFilterString = String.Empty;
        private String _columnGenreFilterString = String.Empty;
        private String _columnInventoryNumberFilterString = String.Empty;
        private String _columnBBKFilterString = String.Empty;
        private String _columnNumberOfAvailableBooksFilterString = String.Empty;
        private String _columnIsObligatoryCopyFilterString = String.Empty;
        private String _columnLocationOfBookNameFilterString = String.Empty;
        private String _columnGrifFilterString = String.Empty;
        private String _columnBarcodeFilterString = String.Empty;
        private String _columnCopyrightMarkFilterString = String.Empty;
        private String _columnEditorFilterString = String.Empty;
        private String _columnCompilerFilterString = String.Empty;

        private RelayCommand _buttonCreateBook;
        private RelayCommand _buttonCloneBook;
        private RelayCommand _buttonEditBook;
        private RelayCommand _buttonEditBookPreload;
        private RelayCommand _buttonDeleteBook;
        private RelayCommand _buttonCreateBarcode;
        private RelayCommand _buttonClearFilters;
        

        public delegate void DoSomething();
        public event DoSomething NotifyAddOrEditNewBook;
        public event DoSomething NotifyCreateBarcodeBook;
        public event DoSomething NotifyOpenEditBookWindow;

        #region СВОЙСТВА
        /// <summary>
        /// Книга на добавление
        /// </summary>
        public Book NewBook { get; set; }
        /// <summary>
        /// Книга на редактирование
        /// </summary>
        public Book EditBook { get; set; }

        /// <summary>
        /// Коллекция книг. Содержит полный перечень книг и их свойства.
        /// </summary>
        public ObservableCollection<Book> CatalogOfBooks {
            get => _catalogOfBooks;
            set {
                _catalogOfBooks = value;
            }
        }

        /// <summary>
        /// Изображение штрихкода
        /// </summary>
        public BitmapImage BarcodeImage
        {
            get => _barcodeImage;
            set
            {
                _barcodeImage = value;
                //OnPropertyChanged(nameof(BarcodeImage));
            }
        }

        /// <summary>
        /// Текущая выбранная книга
        /// </summary>
        public Book SelectedBook
        {
            get => _selectedBook;
            set
            {
                //Игнорируем повторный выбор пользователя
                if (_selectedBook == value)
                    return;
                _selectedBook = value;
                OnPropertyChanged(nameof(SelectedBook));
            }
        }

        /// <summary>
        /// Индикатор обязательного экземпляра книги (используется при выборе книги -> последующем редактировании и/или использовании)
        /// </summary>
        public int IsObligatoryCopy_RadioButton
        {
            get => _isObligatoryCopy_RadioButton;
            set
            {
                _isObligatoryCopy_RadioButton = value;
                OnPropertyChanged(nameof(IsObligatoryCopy_RadioButton));
            }
        }

        //Свойства для фильтрации коллекции
        #region СВОЙСТВА ФИЛЬТРОВ
        public String ColumnNameFilterString
        {
            get => _columnNameFilterString;
            set
            {
                _columnNameFilterString = value;
                OnPropertyChanged(nameof(ColumnNameFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnMainAuthorFilterString
        {
            get => _columnMainAuthorFilterString;
            set
            {
                _columnMainAuthorFilterString = value;
                OnPropertyChanged(nameof(ColumnMainAuthorFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnNumberOfBooksFilterString
        {
            get => _columnNumberOfBooksFilterString;
            set
            {
                _columnNumberOfBooksFilterString = value;
                OnPropertyChanged(nameof(ColumnNumberOfBooksFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnOtherAuthorsFilterString
        {
            get => _columnOtherAuthorsFilterString;
            set
            {
                _columnOtherAuthorsFilterString = value;
                OnPropertyChanged(nameof(ColumnOtherAuthorsFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnDescriptionFilterString
        {
            get => _columnDescriptionFilterString;
            set
            {
                _columnDescriptionFilterString = value;
                OnPropertyChanged(nameof(ColumnDescriptionFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnTagsFilterString
        {
            get => _columnTagsFilterString;
            set
            {
                _columnTagsFilterString = value;
                OnPropertyChanged(nameof(ColumnTagsFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnNumberOfPagesFilterString
        {
            get => _columnNumberOfPagesFilterString;
            set
            {
                _columnNumberOfPagesFilterString = value;
                OnPropertyChanged(nameof(ColumnNumberOfPagesFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnISBNFilterString
        {
            get => _columnISBNFilterString;
            set
            {
                _columnISBNFilterString = value;
                OnPropertyChanged(nameof(ColumnISBNFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnPublisherFilterString
        {
            get => _columnPublisherFilterString;
            set
            {
                _columnPublisherFilterString = value;
                OnPropertyChanged(nameof(ColumnPublisherFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnPublicationYearFilterString
        {
            get => _columnPublicationYearFilterString;
            set
            {
                _columnPublicationYearFilterString = value;
                OnPropertyChanged(nameof(ColumnPublicationYearFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnGenreFilterString
        {
            get => _columnGenreFilterString;
            set
            {
                _columnGenreFilterString = value;
                OnPropertyChanged(nameof(ColumnGenreFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnInventoryNumberFilterString
        {
            get => _columnInventoryNumberFilterString;
            set
            {
                _columnInventoryNumberFilterString = value;
                OnPropertyChanged(nameof(ColumnInventoryNumberFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnBBKFilterString
        {
            get => _columnBBKFilterString;
            set
            {
                _columnBBKFilterString = value;
                OnPropertyChanged(nameof(ColumnBBKFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnNumberOfAvailableBooksFilterString
        {
            get => _columnNumberOfAvailableBooksFilterString;
            set
            {
                _columnNumberOfAvailableBooksFilterString = value;
                OnPropertyChanged(nameof(ColumnNumberOfAvailableBooksFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnIsObligatoryCopyFilterString
        {
            get => _columnIsObligatoryCopyFilterString;
            set
            {
                _columnIsObligatoryCopyFilterString = value;
                OnPropertyChanged(nameof(ColumnIsObligatoryCopyFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }

        public String ColumnLocationOfBookNameFilterString
        {
            get => _columnLocationOfBookNameFilterString;
            set
            {
                _columnLocationOfBookNameFilterString = value;
                OnPropertyChanged(nameof(ColumnLocationOfBookNameFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }
        public String ColumnGrifFilterString
        {
            get => _columnGrifFilterString;
            set
            {
                _columnGrifFilterString = value;
                OnPropertyChanged(nameof(ColumnGrifFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }
        public String ColumnBarcodeFilterString
        {
            get => _columnBarcodeFilterString;
            set
            {
                _columnBarcodeFilterString = value;
                OnPropertyChanged(nameof(ColumnBarcodeFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }
        public String ColumnCopyrightMarkFilterString
        {
            get => _columnCopyrightMarkFilterString;
            set
            {
                _columnCopyrightMarkFilterString = value;
                OnPropertyChanged(nameof(ColumnCopyrightMarkFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }
        public String ColumnEditorFilterString
        {
            get => _columnEditorFilterString;
            set
            {
                _columnEditorFilterString = value;
                OnPropertyChanged(nameof(ColumnEditorFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }
        public String ColumnCompilerFilterString
        {
            get => _columnCompilerFilterString;
            set
            {
                _columnCompilerFilterString = value;
                OnPropertyChanged(nameof(ColumnCompilerFilterString));
                CatalogOfBooksCollection.Filter += Filter;
            }
        }
        #endregion

        /// <summary>
        /// Интерфейс для реализации коллекции
        /// </summary>
        public ICollectionView CatalogOfBooksCollection
        {
            get => _catalogOfBooksCollection;
            set
            {
                _catalogOfBooksCollection = value;
                OnPropertyChanged(nameof(CatalogOfBooksCollection));
            }
        }

        #endregion

        #region ОБСЛУЖИВАЮЩИЕ МЕТОДЫ
        /// <summary>
        /// Предикат фильтрации коллекции книг
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        private bool Filter(object emp)
        {
            Book book = emp as Book;

            return (book.Name.ToLower().Contains(ColumnNameFilterString.ToLower())
                && book.MainAuthor.ToLower().Contains(ColumnMainAuthorFilterString.ToLower())
                && book.NumberOfBooks.ToString().ToLower().Contains(ColumnNumberOfBooksFilterString.ToLower())
                && book.OtherAuthors.ToLower().Contains(ColumnOtherAuthorsFilterString.ToLower())
                && book.Description.ToLower().Contains(ColumnDescriptionFilterString.ToLower())
                && book.Tags.ToLower().Contains(ColumnTagsFilterString.ToLower())
                && book.NumberOfPages.ToString().ToLower().Contains(ColumnNumberOfPagesFilterString.ToLower())
                && book.ISBN.ToLower().Contains(ColumnISBNFilterString.ToLower())
                && book.Publisher.ToLower().Contains(ColumnPublisherFilterString.ToLower())
                && book.PublicationYear.ToString().ToLower().Contains(ColumnPublicationYearFilterString.ToLower())
                && book.Genre.ToLower().Contains(ColumnGenreFilterString.ToLower())
                && book.InventoryNumber.ToLower().Contains(ColumnInventoryNumberFilterString.ToLower())
                && book.BBK.ToLower().Contains(ColumnBBKFilterString.ToLower())
                && book.NumberOfAvailableBooks.ToString().ToLower().Contains(ColumnNumberOfAvailableBooksFilterString.ToLower())
                && book.IsObligatoryCopy.ToLower().Contains(ColumnIsObligatoryCopyFilterString.ToLower())
                && book.LocationOfBookName.ToLower().Contains(ColumnLocationOfBookNameFilterString.ToLower())
                && book.Grif.ToLower().Contains(ColumnGrifFilterString.ToLower())
                && book.Barcode.ToLower().Contains(ColumnBarcodeFilterString.ToLower())
                && book.CopyrightMark.ToLower().Contains(ColumnCopyrightMarkFilterString.ToLower())
                && book.Editor.ToLower().Contains(ColumnEditorFilterString.ToLower())
                && book.Compiler.ToLower().Contains(ColumnCompilerFilterString.ToLower())
                );
        }

        public void UpdateFromDB()
        {
            //Обновляем представление книг из базы
            CatalogOfBooks = Books.GetBooksFromDB();

            if (CatalogOfBooks == null)
            {
                MessageBox.Show(
                    "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                    "Ошибка подключения к БД");
            }
            else if (CatalogOfBooks.Any())
            {
                //Если каталог из базы загружен успешно, обновляем и сортируем коллекцию книг, устанавливаем выбранной книгой первую
                SelectedBook = CatalogOfBooks.First();
                CatalogOfBooksCollection = CollectionViewSource.GetDefaultView(CatalogOfBooks);
                CatalogOfBooksCollection.SortDescriptions.Add(new SortDescription("ISBN", ListSortDirection.Ascending));
                CatalogOfBooksCollection.MoveCurrentToFirst();
                SelectedBook = (Book)CatalogOfBooksCollection.CurrentItem;
            }
        }
        #endregion

        #region ОБРАБОТКА КНОПОК
        /// <summary>
        /// Кнопка создания книги (принятие изменений в окне создания книги)
        /// </summary>
        public RelayCommand ButtonCreateBook
        {
            get
            {
                return _buttonCreateBook ??
                       (_buttonCreateBook = new RelayCommand(obj =>
                       {
                           //Присваиваем книге выбранный режим обязательного экземпляра
                           if (IsObligatoryCopy_RadioButton == 1)
                               NewBook.IsObligatoryCopy = "Да";
                           else
                               NewBook.IsObligatoryCopy = "Нет";

                           //Добавляем книгу в базу
                           Books.AddNewBookToDB(NewBook);

                           //Обновляем представление книг из базы
                           CatalogOfBooks = Books.GetBooksFromDB();
                           if (CatalogOfBooks == null)
                           {
                               MessageBox.Show(
                                   "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                                   "Ошибка подключения к БД");
                           }
                           else if (CatalogOfBooks.Any())
                           {
                               //Если каталог из базы загружен успешно, устанавливаем добавленную книгу текущей
                               SelectedBook = CatalogOfBooks.FirstOrDefault(x => x.Name == NewBook.Name);
                               //Обновляем коллекцию книг, устанавливаем сортировку
                               CatalogOfBooksCollection = CollectionViewSource.GetDefaultView(CatalogOfBooks);
                               CatalogOfBooksCollection.SortDescriptions.Add(new SortDescription("ISBN", ListSortDirection.Ascending));
                               //Устанавливаем выбранной книгой первую в отсортированной коллекции
                               CatalogOfBooksCollection.MoveCurrentTo(CatalogOfBooks.FirstOrDefault(x => x.Name == NewBook.Name));
                               SelectedBook = (Book)CatalogOfBooksCollection.CurrentItem;
                           }

                           //Обнуляем новую книгу 
                           NewBook = new Book();
                           //Закрываем окно
                           NotifyAddOrEditNewBook?.Invoke();

                       }, obj =>
                       {
                           //Кнопка доступна только если заполнено поле "Название книги"
                           if (String.IsNullOrWhiteSpace(NewBook.Name) == true)
                               return false;
                           else return true;
                       }));
            }
        }

        /// <summary>
        /// Кнопка создания книги (принятие изменений в окне создания книги)
        /// </summary>
        public RelayCommand ButtonCloneBook
        {
            get
            {
                return _buttonCloneBook ??
                       (_buttonCloneBook = new RelayCommand(obj =>
                       {
                           //Дублируем выбранную книгу
                           NewBook = (Book)SelectedBook.Clone();

                           //Увеличиваем у копии Инвентарный номер на +1 (т.к. это уникальный признак для книги)
                           try
                           {
                               NewBook.InventoryNumber = (Int32.Parse(NewBook.InventoryNumber)+1).ToString();
                           }
                           catch (FormatException)
                           {
                               NewBook.InventoryNumber = String.Empty;
                           }

                           //Дублированная книга не должна являться обязательным экземпляром
                           NewBook.IsObligatoryCopy = "Нет";

                           //Дублированная книга не добавляет новых тэгов в группу тегов ISBN
                           NewBook.Tags = String.Empty;

                           //Добавляем книгу в базу
                           Books.AddNewBookToDB(NewBook);

                           //Обновляем представление книг из базы
                           CatalogOfBooks = Books.GetBooksFromDB();
                           if (CatalogOfBooks == null)
                           {
                               MessageBox.Show(
                                   "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                                   "Ошибка подключения к БД");
                           }
                           else if (CatalogOfBooks.Any())
                           {
                               //Если каталог из базы загружен успешно, устанавливаем добавленную книгу текущей
                               SelectedBook = CatalogOfBooks.FirstOrDefault(x => x.Name == NewBook.Name);
                               //Обновляем коллекцию книг, устанавливаем сортировку
                               CatalogOfBooksCollection = CollectionViewSource.GetDefaultView(CatalogOfBooks);
                               CatalogOfBooksCollection.SortDescriptions.Add(new SortDescription("ISBN", ListSortDirection.Ascending));
                               //Устанавливаем выбранной книгой первую в отсортированной коллекции
                               CatalogOfBooksCollection.MoveCurrentTo(CatalogOfBooks.FirstOrDefault(x => x.Name == NewBook.Name));
                               SelectedBook = (Book)CatalogOfBooksCollection.CurrentItem;
                           }

                           //Обнуляем новую книгу 
                           NewBook = new Book();

                       }, obj =>
                       {
                           //Кнопка недоступна, если не выбрана ни одна книга
                           if (!(SelectedBook == null))
                               return true;
                           else
                               return false;
                       }));
            }
        }

        /// <summary>
        /// Кнопка редактирования книги на основной панели каталога (открытие окна редактирования)
        /// </summary>
        public RelayCommand ButtonEditBookPreload
        {
            get
            {
                return _buttonEditBookPreload ??
                       (_buttonEditBookPreload = new RelayCommand(obj =>
                       {
                           //Избавляемся от ссылочной привязки к объекту выбранной книги (на случай отмены изменений)
                           EditBook = (Book)SelectedBook.Clone();

                           //Определяем, является ли выбранная книга обязательным экземпляром
                           if (EditBook.IsObligatoryCopy.Contains("Да")) IsObligatoryCopy_RadioButton = 1;
                           else IsObligatoryCopy_RadioButton = 0;

                           //Уведомляем GUI о необходимости создания окна редактирования книги
                           NotifyOpenEditBookWindow?.Invoke();

                       }, obj =>
                       {
                           //Кнопка недоступна, если не выбрана ни одна книга
                           if (!(SelectedBook == null))
                               return true;
                           else
                               return false;
                       }));
            }
        }

        /// <summary>
        /// Кнопка редактирования книги (принятие изменений в окне редактирования книги)
        /// </summary>
        public RelayCommand ButtonEditBook
        {
            get
            {
                return _buttonEditBook ??
                       (_buttonEditBook = new RelayCommand(obj =>
                       {
                           //Присваиваем книге выбранный режим обязательного экземпляра
                           if (IsObligatoryCopy_RadioButton == 1)
                               EditBook.IsObligatoryCopy = "Да";
                           else
                               EditBook.IsObligatoryCopy = "Нет";

                           //Изменяем книгу в базе
                           Books.EditBookToDB(EditBook);

                           //Обновляем представление книг из базы
                           CatalogOfBooks = Books.GetBooksFromDB();
                           if (CatalogOfBooks == null)
                           {
                               MessageBox.Show(
                                   "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                                   "Ошибка подключения к БД");
                           }
                           else if (CatalogOfBooks.Any())
                           {
                               //Если каталог из базы загружен успешно, устанавливаем измененную книгу текущей
                               SelectedBook = CatalogOfBooks.FirstOrDefault(x => x.Id == SelectedBook.Id);
                               //Обновляем коллекцию книг, устанавливаем сортировку
                               CatalogOfBooksCollection = CollectionViewSource.GetDefaultView(CatalogOfBooks);
                               CatalogOfBooksCollection.SortDescriptions.Add(new SortDescription("ISBN", ListSortDirection.Ascending));
                               //Устанавливаем выбранной книгой первую в отсортированной коллекции
                               CatalogOfBooksCollection.MoveCurrentTo(CatalogOfBooks.FirstOrDefault(x => x.Id == SelectedBook.Id));
                               SelectedBook = (Book)CatalogOfBooksCollection.CurrentItem;
                           }

                           //Закрываем окно
                           NotifyAddOrEditNewBook?.Invoke();

                       }, obj =>
                       {
                           //Если книга не выбрана или имя не задано - кнопка неактивна
                           if (!(EditBook == null))
                           {
                               if (String.IsNullOrWhiteSpace(EditBook.Name) == true)
                                   return false;
                               else return true;
                           }
                           else return false;
                       }));
            }
        }

        /// <summary>
        /// Кнопка удаления книги (на основной панели каталога)
        /// </summary>
        public RelayCommand ButtonDeleteBook
        {
            get
            {
                return _buttonDeleteBook ??
                       (_buttonDeleteBook = new RelayCommand(obj =>
                       {
                           var result = MessageBox.Show(
                               $"Вы уверены, что хотите удалить книгу '{SelectedBook.Name}'?",
                               "Подтвердите удаление книги",
                                 MessageBoxButton.YesNo,
                                 MessageBoxImage.Question);

                           // If the no button was pressed ...
                           if (result == MessageBoxResult.Yes)
                           {
                               //Если получено подтверждение удаления книги, удаляем книгу из базы
                               Books.DeleteBookToDB(SelectedBook);

                               //Обновляем представление книг из базы
                               CatalogOfBooks = Books.GetBooksFromDB();
                               if (CatalogOfBooks == null)
                               {
                                   MessageBox.Show(
                                       "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                                       "Ошибка подключения к БД");
                               }
                               else if (CatalogOfBooks.Any())
                               {
                                   //Если каталог из базы загружен успешно, устанавливаем измененную книгу текущей
                                   SelectedBook = CatalogOfBooks.First();
                                   //Если каталог из базы загружен успешно, обновляем коллекцию книг, устанавливаем сортировку
                                   CatalogOfBooksCollection = CollectionViewSource.GetDefaultView(CatalogOfBooks);
                                   CatalogOfBooksCollection.SortDescriptions.Add(new SortDescription("ISBN", ListSortDirection.Ascending));
                                   //Устанавливаем выбранной книгой первую в отсортированной коллекции
                                   CatalogOfBooksCollection.MoveCurrentToFirst();
                                   SelectedBook = (Book)CatalogOfBooksCollection.CurrentItem;
                               }
                           }
                       }, obj =>
                       {
                           //Кнопка недоступна, если не выбрана ни одна книга
                           if (!(SelectedBook == null))
                               return true;
                           else
                               return false;
                       }));
            }
        }

        /// <summary>
        /// Кнопка создания штрихкода (на основной панели каталога)
        /// </summary>
        public RelayCommand ButtonCreateBarcodeBook
        {
            get
            {
                return _buttonCreateBarcode ??
                       (_buttonCreateBarcode = new RelayCommand(obj =>
                       {
                           //Генерируем штрихкод выбранной книги
                           BarcodeImage = Books.GetBarcodeImage(SelectedBook.Barcode);

                           //Открываем окно отображения штрихкода
                           NotifyCreateBarcodeBook?.Invoke();
                       }, obj =>
                       {
                           //Кнопка недоступна, если не выбрана ни одна книга
                           if (!(SelectedBook == null))
                               return true;
                           else 
                               return false;
                       }));
            }
        }

        /// <summary>
        /// Кнопка сброса фильтров поиска
        /// </summary>
        public RelayCommand ButtonClearFilters
        {
            get
            {
                return _buttonClearFilters ??
                       (_buttonClearFilters = new RelayCommand(obj =>
                       {
                           //Очистка фильтров поиска
                           ColumnNameFilterString = String.Empty;
                           ColumnMainAuthorFilterString = String.Empty;
                           ColumnNumberOfBooksFilterString = String.Empty;
                           ColumnOtherAuthorsFilterString = String.Empty;
                           ColumnDescriptionFilterString = String.Empty;
                           ColumnTagsFilterString = String.Empty;
                           ColumnNumberOfPagesFilterString = String.Empty;
                           ColumnISBNFilterString = String.Empty;
                           ColumnPublisherFilterString = String.Empty;
                           ColumnPublicationYearFilterString = String.Empty;
                           ColumnGenreFilterString = String.Empty;
                           ColumnInventoryNumberFilterString = String.Empty;
                           ColumnBBKFilterString = String.Empty;
                           ColumnNumberOfAvailableBooksFilterString = String.Empty;
                           ColumnIsObligatoryCopyFilterString = String.Empty;
                           ColumnLocationOfBookNameFilterString = String.Empty;
                           ColumnGrifFilterString = String.Empty;
                           ColumnBarcodeFilterString = String.Empty;
                           ColumnCopyrightMarkFilterString = String.Empty;
                           ColumnEditorFilterString = String.Empty;
                           ColumnCompilerFilterString = String.Empty;
                       }, obj =>
                       {
                            return true;
                       }));
            }
        }
        #endregion

        #region КОНСТРУКТОР
        public Menu_CatalogVM()
        {
            //Инициируем объект новой книги
            NewBook = new Book();

            UpdateFromDB();
        }
        #endregion
    }

}

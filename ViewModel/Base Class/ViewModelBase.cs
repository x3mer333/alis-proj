﻿using ALIS_Proj.Model.Entities;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace ALIS_Proj.ViewModel
{
    public class ViewModelBase : INotifyPropertyChanged, IDataErrorInfo
    {
        public string Error => throw new System.NotImplementedException();

        public string this[string columnName] => throw new System.NotImplementedException();

        #region INotifyPropertyChanged Members

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
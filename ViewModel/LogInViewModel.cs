﻿using ALIS_Proj.Model;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ALIS_Proj.Model.Entities;
using ALIS_Proj.Model.Enums;

namespace ALIS_Proj.ViewModel
{
    public class LogInViewModel : ViewModelBase
    {
        private bool _errorConnectingToDataBase;
        private bool _isButtonEnterEnabled;
        private string _barcodeFromForm;
        private Person _selectedPerson;
        private RelayCommand _buttonEnter;
        private RelayCommand _logoClicked;

        #region ОПИСАНИЕ СВОЙСТВ
        /// <summary>
        /// В это свойство передается пароль с PasswordBox. Понимаю, что это противоречит политики безопасности (хранить пароли в переменных), но для текущий задачи это несущественно.
        /// </summary>
        public string PasswordFromForm { get; set; }

        /// <summary>
        /// Коллекция пользователей. Содержит полный набор сведений о персонале.
        /// </summary>
        public ObservableCollection<Person> Persons { get; set; }

        /// <summary>
        /// Принимает True при ошибке соединения с БД, влияет на доступность кнопки проверки авторизации пользователя
        /// </summary>
        public bool ErrorConnectingToDataBase
        {
            get => _errorConnectingToDataBase;
            set
            {
                _errorConnectingToDataBase = value;
                //Кнопка входа недоступна, если есть ошибка соединения с базой данных.
                if (_errorConnectingToDataBase) IsButtonEnterEnabled = false;
                OnPropertyChanged(nameof(ErrorConnectingToDataBase));
            }
        }

        /// <summary>
        /// Определяет доступность кнопки "Вход"
        /// </summary>
        public bool IsButtonEnterEnabled
        {
            get => _isButtonEnterEnabled;
            set
            {
                _isButtonEnterEnabled = value;
                OnPropertyChanged(nameof(IsButtonEnterEnabled));
            }
        }

        /// <summary>
        /// В свойстве хранится штрихкод отсканированный пользователем
        /// </summary>
        public string BarcodeFromForm
        {
            get => _barcodeFromForm;
            set
            {
                if (Persons.Any(x => x.Barcode == value))
                {
                    //Меняем текущего пользователя
                    SelectedPerson = Persons.FirstOrDefault(x => x.Barcode == value);
                    PasswordFromForm = SelectedPerson.Password;
                }

                _barcodeFromForm = value;
                OnPropertyChanged(nameof(BarcodeFromForm));
            }
        }

        /// <summary>
        /// Объект Person выбранный в ComboBox
        /// </summary>
        public Person SelectedPerson
        {
            get => _selectedPerson;
            set
            {
                //Игнорируем повторный выбор пользователя
                if (_selectedPerson == value) return;
                _selectedPerson = value;
                OnPropertyChanged(nameof(SelectedPerson));
            }
        }
        #endregion

        #region КОНСТРУКТОР
        public LogInViewModel()
        {
            PasswordFromForm = string.Empty;

            Persons = Staff.GetPersonsFromDB(AccessRole.Admin);
            if (Persons == null)
            {
                MessageBox.Show(
                    "Не удалось установить соединение с базой данных.\nТребуется проверить строку подключения в конфигурационном файле и работоспособность сервера баз данных.\nПосле чего перезапустите программу.",
                    "Ошибка подключения к БД");
                ErrorConnectingToDataBase = true;
            }
            else if (Persons.Any())
            {
                SelectedPerson = Persons.First();
                ErrorConnectingToDataBase = false;
            }
        }
        #endregion

        #region ОБРАБОТКА КНОПОК
        /// <summary>
        /// Кнопка вход
        /// </summary>
        public RelayCommand ButtonEnter
        {
            get
            {
                return _buttonEnter ??
                       (_buttonEnter = new RelayCommand(obj =>
                       {
                           //Логику открытия/закрытия окна оствляем CodeBehind
                       }, obj =>
                       {
                           if (SelectedPerson == null) return false;
                           //Проверка логина и пароля. Если совпадает, кнопка становится доступна.
                           return PasswordFromForm.Equals(SelectedPerson.Password);
                       }));
            }
        }

        /// <summary>
        /// Модульное окно "О программе"
        /// </summary>
        public RelayCommand LogoClicked
        {
            get
            {
                return _logoClicked ??
                       (_logoClicked = new RelayCommand(obj =>
                       {
                           MessageBox.Show(
                               "Спасибо, что пользуетесь программой!\n" +
                               "По всем вопросам и предложениям обращайтесь по адресу:\n" +
                               "x3mer333@gmail.com","О программе");
                       }, obj => true));
            }
        }


        //Сохранил для себя кусок кода как пример, чтобы потом можно было посмотреть и вспомнить. К программе отношения не имеет.
        /*public ICommand SaveCommand
        {
            get
            {
                return new ActionCommand(action => Save(),
            canExecute => CanSave());
            }
        }
        */
        #endregion
    }
}
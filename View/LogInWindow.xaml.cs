﻿using ALIS_Proj.View.MainWindowV;
using ALIS_Proj.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ALIS_Proj.View
{
    /// <summary>
    /// Логика взаимодействия для LogInWindow.xaml
    /// </summary>
    public partial class LogInWindow : Window
    {
        public LogInWindow()
        {
            InitializeComponent();

            var VM = new LogInViewModel();
            DataContext = VM;

            TextBoxBarcode.Focus();
        }

        //Создание обработчика нажатия мыши обходится существенно дешевле реализации в концепте MVVM (тем более ущерб не большой, т.к. бизнес логику не затрагивает).
        private void Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        //Создание обработчика выхода из приложения обходится существенно дешевле реализации в концепте MVVM (тем более ущерб не большой, т.к. бизнес логику не затрагивает).
        private void Button_Click_Exit(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //PasswordBox не предоставляет DP для Password. Привязать к свойству стандартным биндингом возможности нет.
        //Нижеприведенный код нарушает принципы строгой безопасности, но в рамках нашего приложения это приемлемые риски.
        private void PasswordBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (this.DataContext != null)
            {
                ((LogInViewModel)this.DataContext).PasswordFromForm = ((PasswordBox)sender).Password;
            }
        }

        private void PasswordBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) ButtonEnter_Click(null, null);
        }

        private void ButtonEnter_Click(object sender, RoutedEventArgs e)
        {
            if (ButtonEnter.IsEnabled)
            {
                MainWindow mainWindow = new MainWindow();
                mainWindow.Show();
                this.Close();
            }

        }

        private void TextBoxBarcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter) ButtonEnter_Click(null, null);
        }
    }
}

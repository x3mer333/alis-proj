﻿using ALIS_Proj.ViewModel.MainWindowVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ALIS_Proj.View.MainWindowV.UsersAccounting
{
    /// <summary>
    /// Логика взаимодействия для UsersAccounting_FindPerson.xaml
    /// </summary>
    public partial class UsersAccounting_FindPerson : Window
    {
        public UsersAccounting_FindPerson()
        {
            InitializeComponent();
        }
        private void Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void CloseWindow()
        {
            this.Close();
        }

        private void Tools_FindPerson_Loaded(object sender, RoutedEventArgs e)
        {
            ((Menu_UserAccountingVM)this.DataContext).NotifyFindPersonClose += CloseWindow;
        }
    }
}

﻿using ALIS_Proj.ViewModel.MainWindowVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ALIS_Proj.View.MainWindowV.UsersAccounting
{
    /// <summary>
    /// Логика взаимодействия для UsersAccounting_AddUser.xaml
    /// </summary>
    public partial class UsersAccounting_AddUser : Window
    {
        public UsersAccounting_AddUser()
        {
            InitializeComponent();
        }

        private void Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            this.Close();
        }

        private void Tools_AddBook_Loaded(object sender, RoutedEventArgs e)
        {
            //Устанавливаем значение по умолчанию для комбобокса "роль добавленного пользователя"
            ComboBox_NewPersonAccessRole.SelectedIndex = 0;

            //Привязываем закрытие окна к событию VM
            ((Menu_UserAccountingVM)this.DataContext).NotifyCreatePerson += CloseWindow;
        }
    }
}

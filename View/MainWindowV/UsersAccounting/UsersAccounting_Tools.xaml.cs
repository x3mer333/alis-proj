﻿using ALIS_Proj.ViewModel.MainWindowVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ALIS_Proj.View.MainWindowV.UsersAccounting
{
    /// <summary>
    /// Логика взаимодействия для UsersAccounting_Tools.xaml
    /// </summary>
    public partial class UsersAccounting_Tools : UserControl
    {
        UsersAccounting_AddUser addPersonWindow;
        UsersAccounting_EditUser editPersonWindow;
        UsersAccounting_BarcodeCreate barcodeCreateWindow;
        UsersAccounting_FindPerson findPersonWindow;

        public UsersAccounting_Tools()
        {
            InitializeComponent();
        }

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {
            ((Menu_UserAccountingVM)DataContext).NotifyCreatePersonPreload += AddUserShowWindow;
            ((Menu_UserAccountingVM)DataContext).NotifyEditPersonPreload += EditUserShowWindow;
            ((Menu_UserAccountingVM)DataContext).NotifyBarcodeCreatePreload += BarcodeCreateShowWindow;
            ((Menu_UserAccountingVM)DataContext).NotifyFindPersonPreload += FindUserShowWindow;
        }

        private void AddUserShowWindow()
        {
            addPersonWindow = new UsersAccounting_AddUser();
            addPersonWindow.DataContext = this.DataContext;
            addPersonWindow.ShowDialog();
        }

        private void EditUserShowWindow()
        {
            editPersonWindow = new UsersAccounting_EditUser();
            editPersonWindow.DataContext = this.DataContext;
            editPersonWindow.ShowDialog();
        }

        private void BarcodeCreateShowWindow()
        {
            barcodeCreateWindow = new UsersAccounting_BarcodeCreate();
            barcodeCreateWindow.DataContext = this.DataContext;
            barcodeCreateWindow.ShowDialog();
        }

        private void FindUserShowWindow()
        {
            findPersonWindow = new UsersAccounting_FindPerson();
            findPersonWindow.DataContext = this.DataContext;
            findPersonWindow.ShowDialog();
        }
    }
}

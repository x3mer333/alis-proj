﻿using ALIS_Proj.ViewModel.MainWindowVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ALIS_Proj.View.MainWindowV
{
    /// <summary>
    /// Логика взаимодействия для Menu_Catalog_Tools.xaml
    /// </summary>
    public partial class Menu_Catalog_Tools : UserControl
    {
        Menu_Catalog_Tools_AddBook addBookWindow;
        Menu_Catalog_Tools_EditBook editBookWindow;
        Menu_Catalog_Tools_BarcodeCreate barcodeCreateWindow;
        Menu_Catalog_Tools_FindBook menu_Catalog_Tools_FindBook;

        public Menu_Catalog_Tools()
        {
            InitializeComponent();
        }

        private void Button_AddBook_Click(object sender, RoutedEventArgs e)
        {
            addBookWindow = new Menu_Catalog_Tools_AddBook();
            addBookWindow.DataContext = this.DataContext;
            addBookWindow.ShowDialog();
        }

        private void ShowEditBookMenu()
        {
            editBookWindow = new Menu_Catalog_Tools_EditBook();
            editBookWindow.DataContext = this.DataContext;
            editBookWindow.ShowDialog();
        }

        private void Button_FindBook_Click(object sender, RoutedEventArgs e)
        {
            menu_Catalog_Tools_FindBook = new Menu_Catalog_Tools_FindBook();
            menu_Catalog_Tools_FindBook.DataContext = this.DataContext;
            menu_Catalog_Tools_FindBook.ShowDialog();
        }

        private void ShowBarcodeMenu()
        {
            barcodeCreateWindow = new Menu_Catalog_Tools_BarcodeCreate();
            barcodeCreateWindow.DataContext = this.DataContext;
            barcodeCreateWindow.ShowDialog();
        }

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {
            ((Menu_CatalogVM)DataContext).NotifyCreateBarcodeBook += ShowBarcodeMenu;
            ((Menu_CatalogVM)DataContext).NotifyOpenEditBookWindow += ShowEditBookMenu;
        }
    }
}

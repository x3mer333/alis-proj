﻿using ALIS_Proj.ViewModel.MainWindowVM;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ALIS_Proj.View.MainWindowV
{
    /// <summary>
    /// Логика взаимодействия для Menu_Catalog_Tools_BarcodeCreate.xaml
    /// </summary>
    public partial class Menu_Catalog_Tools_BarcodeCreate : Window
    {
        public Menu_Catalog_Tools_BarcodeCreate()
        {
            InitializeComponent();
        }
        private void Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Catalog_Tools_BarcodeCreate_Loaded(object sender, RoutedEventArgs e)
        {
            ((Menu_CatalogVM)this.DataContext).NotifyAddOrEditNewBook += CloseWindow;
        }

        private void CloseWindow()
        {
            this.Close();
        }

        private void Button_Save_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "barcode";
            dlg.DefaultExt = ".png";
            dlg.Filter = "Png Image|*.png";

            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                var bitmapTemp = new WriteableBitmap((BitmapSource)barcodeImage.Source);
                var encoder = new PngBitmapEncoder(); // Or PngBitmapEncoder, or whichever encoder you want
                encoder.Frames.Add(BitmapFrame.Create(bitmapTemp));
                using (var stream = dlg.OpenFile())
                {
                    encoder.Save(stream);
                }
            }

        }

        private void Button_Clouse_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {


            System.Windows.Forms.PrintDialog printDialog1 = new System.Windows.Forms.PrintDialog();
            System.Drawing.Printing.PrintDocument Document = new System.Drawing.Printing.PrintDocument();
            Document.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(Document_PrintPage);
            DialogResult result = printDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                Document.Print();
            }
        }
        void Document_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Bitmap bmpOut = null;

            using (MemoryStream ms = new MemoryStream())
            {
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create((BitmapSource)this.barcodeImage.Source));
                encoder.Save(ms);

                using (Bitmap bmp = new Bitmap(ms))
                {
                    bmpOut = new Bitmap(bmp);
                }
            }

            e.Graphics.DrawImage(bmpOut, new System.Drawing.Point(0, 0)); //Картинка на печать
        }
    }
}

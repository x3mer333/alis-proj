﻿using ALIS_Proj.ViewModel.MainWindowVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ALIS_Proj.View.MainWindowV
{
    /// <summary>
    /// Логика взаимодействия для Menu_Catalog_Tools_AddBook.xaml
    /// </summary>
    public partial class Menu_Catalog_Tools_AddBook : Window
    {
        public Menu_Catalog_Tools_AddBook()
        {
            InitializeComponent();
        }

        private void Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            this.Close();
        }

        private void Catalog_Tools_AddBook_Loaded(object sender, RoutedEventArgs e)
        {
            ((Menu_CatalogVM)this.DataContext).NotifyAddOrEditNewBook += CloseWindow;
            RadioButtonNo_IsObligatoryCopy.IsChecked = true;
        }
    }
}

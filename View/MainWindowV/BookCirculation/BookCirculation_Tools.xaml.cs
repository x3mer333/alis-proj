﻿using ALIS_Proj.ViewModel.MainWindowVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ALIS_Proj.View.MainWindowV.BookCirculation
{
    /// <summary>
    /// Логика взаимодействия для BookCirculation_Tools.xaml
    /// </summary>
    public partial class BookCirculation_Tools : UserControl
    {
        BookCirculation_GiveBookByBarcode giveBookByBarcodeWindow;
        public BookCirculation_Tools()
        {
            InitializeComponent();
        }
        private void ShowGiveBookByBarcodeWindow()
        {
            giveBookByBarcodeWindow = new BookCirculation_GiveBookByBarcode();
            giveBookByBarcodeWindow.DataContext = this.DataContext;
            giveBookByBarcodeWindow.ShowDialog();
        }

        private void StackPanel_Loaded(object sender, RoutedEventArgs e)
        {
            ((Menu_BookCirculationVM)DataContext).NotifyGiveBookByBarcodePreload += ShowGiveBookByBarcodeWindow;
        }
    }
}

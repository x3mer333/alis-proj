﻿using ALIS_Proj.ViewModel.MainWindowVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ALIS_Proj.View.MainWindowV.BookCirculation
{
    /// <summary>
    /// Логика взаимодействия для BookCirculation_GiveBookByBarcode.xaml
    /// </summary>
    public partial class BookCirculation_GiveBookByBarcode : Window
    {
        public BookCirculation_GiveBookByBarcode()
        {
            InitializeComponent();
        }
        private void Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        void CloseWindow ()
        {
            this.Close();
        }

        private void Tools_GiveBookByBarcodeWindow_Loaded(object sender, RoutedEventArgs e)
        {
            ((Menu_BookCirculationVM)DataContext).NotifyGiveBookByBarcodeCloseWindow += CloseWindow;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ALIS_Proj.View.MainWindowV
{
    /// <summary>
    /// Логика взаимодействия для Menu_BookCirculation.xaml
    /// </summary>
    public partial class Menu_BookCirculation : UserControl
    {
        public Menu_BookCirculation()
        {
            InitializeComponent();
        }

        private void BodyGrid_Loaded(object sender, RoutedEventArgs e)
        {
            TextBoxPersonBarcode.Focus();
        }

        void DataGrid_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }
    }
}

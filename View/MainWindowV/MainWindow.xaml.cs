﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ALIS_Proj.ViewModel.MainWindowVM;

namespace ALIS_Proj.View.MainWindowV
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Menu_CatalogVM menuCatalogVM;
        Menu_UserAccountingVM menuUserAccountingVM;
        Menu_BookCirculationVM menuBookCirculationVM;

        public MainWindow()
        {
            InitializeComponent();
            //TODO: Сделать нормальную анимацию кнопок управления окном (minimized, maximized, close)

            menuCatalogVM = new Menu_CatalogVM();
            menuUserAccountingVM = new Menu_UserAccountingVM();
            menuBookCirculationVM = new Menu_BookCirculationVM();

            this.DataContext = menuCatalogVM;
            menu_Catalog_Tools.DataContext = this.DataContext;
            userAccounting_Tools.DataContext = menuUserAccountingVM;
            bookCirculation_Tools.DataContext = menuBookCirculationVM;



            UserControlsShow(menu_Catalog_Tools);
        }

        //Создание обработчика нажатия мыши обходится существенно дешевле реализации в концепте MVVM (тем более ущерб не большой, т.к. бизнес логику не затрагивает).
        /// <summary>
        /// Перемещение окна приложения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                base.OnMouseLeftButtonDown(e);
                this.DragMove();
            }
            
        }

        /// <summary>
        /// Кнопка закрытия приложения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonClose_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Кнопка разворачивания приложения на весь экран
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonMaximized_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (WindowState == WindowState.Normal)
                this.WindowState = WindowState.Maximized;
            else
                this.WindowState = WindowState.Normal;
        }

        /// <summary>
        /// Кнопка сворачивания приложения
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonMinimized_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        /// <summary>
        /// Подгружаем DataContext Каталога
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Catalog_Clicked(object sender, RoutedEventArgs e)
        {
            this.DataContext = menuCatalogVM;
            menu_Catalog_Tools.DataContext = this.DataContext;
            ((Menu_CatalogVM)DataContext).UpdateFromDB();
            HeaderText.Text = "Главное окно АБИС: КАТАЛОГ";

            UserControlsShow(menu_Catalog_Tools);
        }
        

        /// <summary>
        /// Подгружаем DataContext Учета пользователей
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserAccounting_Clicked(object sender, RoutedEventArgs e)
        {
            this.DataContext = menuUserAccountingVM;
            userAccounting_Tools.DataContext = this.DataContext;
            HeaderText.Text = "Главное окно АБИС: ЧИТАТЕЛИ";

            UserControlsShow(userAccounting_Tools);
        }

        /// <summary>
        /// Подгружаем DataContext Оборота книг
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BookCirculation_Clicked(object sender, RoutedEventArgs e)
        {
            this.DataContext = menuBookCirculationVM;
            bookCirculation_Tools.DataContext = this.DataContext;
            HeaderText.Text = "Главное окно АБИС: КНИГООБОРОТ";
            
            UserControlsShow(bookCirculation_Tools);
            
        }

        private void MainWindowName_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result =
                  MessageBox.Show(
                    "Вы уверены, что хотите выйти?",
                    "Окно сомнений и прощаний",
                    MessageBoxButton.YesNo,
                    MessageBoxImage.Warning);
            if (result == MessageBoxResult.No)
            {
                // If user doesn't want to close, cancel closure
                e.Cancel = true;
            }
        }

        private void UserControlsShow(UserControl us)
        {
            menu_Catalog_Tools.Visibility = Visibility.Hidden;
            userAccounting_Tools.Visibility = Visibility.Hidden;
            bookCirculation_Tools.Visibility = Visibility.Hidden;

            us.Visibility = Visibility.Visible;
        }
    }
}

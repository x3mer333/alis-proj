﻿using ALIS_Proj.Model.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Windows;

namespace ALIS_Proj.Model
{
    public static class BooksCirculation
    {
        static public void SetBookToPerson (Book book, Person person)
        {
            string sqlQuery = $"INSERT INTO BooksCirculation (IdBook, IdPerson)" +
                $"VALUES({book.Id}" +
                $", {person.Id})" +
                $" ";

            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                MessageBox.Show("Произошла ошибка соединения с БД. Перезапустите программу.", "Ошибка соединения с БД");
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            _ = command.ExecuteReader();

            //Закрываем соединение.
            connectToSql.Close();
        }

        public static ObservableCollection<Book> GetBooksOfPersonFromDB(Person person)
        {
            ObservableCollection<Book> collection = new ObservableCollection<Book>();

            string sqlQuery = $"SELECT b.[Id]"+
                              $",[Name]" +
                              $",[Genre]" +
                              $",[MainAuthor]" +
                              $",[OtherAuthors]" +
                              $",[PublicationYear]" +
                              $",[Publisher]" +
                              $",[ISBN]" +
                              $",[NumberOfPages]" +
                              $",[Description]" +
                              $",[Barcode]" +
                              $",[NumberOfBooks]" +
                              $",[Tags]" +
                              $",[InventoryNumber]" +
                              $",[BBK]" +
                              $",[NumberOfAvailableBooks]" +
                              $",[IsObligatoryCopy]" +
                              $",[LocationOfBookID]" +
                              $",[LocationOfBookName]" +
                              $",[Grif]" +
                          $"FROM(" +
                            $"SELECT *" +
                            $"FROM[dbo].[BooksCirculation]" +
                            $"WHERE IdPerson = {person.Id} " +
                            $"and IsReturned = 0) a " +
                          $"LEFT JOIN Books b ON a.IdBook = b.Id";
            //Строка подключения берется из конфиг. файла
            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                return null;
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            SqlDataReader booksFromDb = command.ExecuteReader();

            while (booksFromDb.Read())
            {
                collection.Add(new Book
                {
                    Id = Convert.ToInt32(booksFromDb["Id"]),
                    Name = booksFromDb["Name"].ToString(),
                    Genre = booksFromDb["Genre"].ToString(),
                    MainAuthor = booksFromDb["MainAuthor"].ToString(),
                    OtherAuthors = booksFromDb["OtherAuthors"].ToString(),
                    PublicationYear = Convert.ToInt32(booksFromDb["PublicationYear"]),
                    Publisher = booksFromDb["Publisher"].ToString(),
                    ISBN = booksFromDb["ISBN"].ToString(),
                    NumberOfPages = Convert.ToInt32(booksFromDb["NumberOfPages"]),
                    Description = booksFromDb["Description"].ToString(),
                    Barcode = booksFromDb["Barcode"].ToString(),
                    Tags = booksFromDb["Tags"].ToString(),
                    NumberOfBooks = Convert.ToInt32(booksFromDb["NumberOfBooks"]),
                    InventoryNumber = booksFromDb["InventoryNumber"].ToString(),
                    BBK = booksFromDb["BBK"].ToString(),
                    NumberOfAvailableBooks = Convert.ToInt32(booksFromDb["NumberOfAvailableBooks"]),
                    IsObligatoryCopy = booksFromDb["IsObligatoryCopy"].ToString(),
                    LocationOfBookID = Convert.ToInt32(booksFromDb["LocationOfBookID"]),
                    LocationOfBookName = booksFromDb["LocationOfBookName"].ToString(),
                    Grif = booksFromDb["Grif"].ToString()
                });
            }

            //Закрываем соединение.
            connectToSql.Close();
            //Соединение с БД успешно.
            return collection;
        }
    }
}

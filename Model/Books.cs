﻿using ALIS_Proj.Model.Entities;
using ALIS_Proj.Properties;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;

using Aspose;
using Aspose.BarCode;
using Aspose.BarCode.Generation;

namespace ALIS_Proj.Model
{
    /// <summary>
    /// Вспомогательный класс для работы с книгами
    /// </summary>
    public static class Books
    {
        /// <summary>
        /// Конвертация Bitmap в формат BitmapImage
        /// </summary>
        /// <param name="bmp">Исходное изображение в формате Bitmap</param>
        /// <returns></returns>
        private static BitmapImage ConverBitmapToBitmapImage(System.Drawing.Bitmap bmp)
        {
            MemoryStream stream = new MemoryStream();
            bmp.Save(stream, ImageFormat.Png);

            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = stream;
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();
            return bitmapImage;
        }

        /// <summary>
        /// Генерирование BitmapImage штрихкода на основе строки
        /// </summary>
        /// <param name="barcodeString">Строка штрихкода</param>
        /// <returns></returns>
        public static BitmapImage GetBarcodeImage (string barcodeString)
        {
            Aspose.BarCode.BarCodeBuilder barcode = new BarCodeBuilder(barcodeString, EncodeTypes.Code128);
            barcode.CodeLocation = Aspose.BarCode.CodeLocation.None;
            //barcode.GetOnlyBarCodeImage
            barcode.xDimension = 0.5f;
            barcode.GraphicsUnit = GraphicsUnit.Millimeter;
            barcode.AutoSize = false;
            barcode.BorderWidth = 0f;
            System.Drawing.SizeF minSize = barcode.GetMinimumBarCodeSize();
            barcode.ImageWidth = minSize.Width;
            barcode.ImageHeight = minSize.Height;


            //Настраиваем объект штрихкода (BarcodeLib)
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.BackColor = System.Drawing.Color.White;
            b.ForeColor = System.Drawing.Color.Black;
            b.IncludeLabel = false;
            b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
            b.Alignment = BarcodeLib.AlignmentPositions.LEFT;
            b.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;
            System.Drawing.Font font = new System.Drawing.Font("verdana", 10f);
            b.LabelFont = font;
            b.Width = 224;
            b.Height = 30;
            
            //Загружаем фон картинки
            Bitmap tempBitmap = new Bitmap(Resources.BookBarcodeFrameLarge);//(new BarcodeLib.Barcode().Encode(BarcodeLib.TYPE.CODE128B, barcodeString));
            //Создаем картинку штрихкода
            //Bitmap tempBitmap2 = new Bitmap(b.Encode(BarcodeLib.TYPE.CODE128, barcodeString));
            Bitmap tempBitmap2 = new Bitmap(barcode.BarCodeImage);
            //Усекаем белую рамку справа (слева нету)
            tempBitmap2 = tempBitmap2.Clone(new Rectangle(0, 45, tempBitmap2.Width, 10), System.Drawing.Imaging.PixelFormat.DontCare);
            //Создаем графическое полотно для рисования и объеденения изображений
            Graphics graphicImage = Graphics.FromImage(tempBitmap);

            //Задаем отрисовку со сглаживанием
            graphicImage.SmoothingMode = SmoothingMode.AntiAlias;
            graphicImage.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            graphicImage.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            graphicImage.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            //Объединяем фон и штрихкод
            graphicImage.DrawImage(tempBitmap2, new Rectangle(new System.Drawing.Point(12, 35), new System.Drawing.Size(202, 27)));

            //Добавляем надпись под штрихкодом (Встроенный IncludeLabel в BarcodeLib не устроил)
            graphicImage.DrawString(barcodeString + "                                        г. Северск", new Font("Arial", 7, System.Drawing.FontStyle.Bold), SystemBrushes.WindowText, new System.Drawing.Point(12, 62));

            Console.WriteLine("tempBitmap1 width = {0}", tempBitmap.Width);
            Console.WriteLine("tempBitmap1 height = {0}", tempBitmap.Height);
            Console.WriteLine("tempBitmap2 width = {0}", tempBitmap2.Width);
            Console.WriteLine("tempBitmap2 height = {0}", tempBitmap2.Height);

            //Конвертируем в требуемый формат BitmapImage
            BitmapImage barcodeImage = ConverBitmapToBitmapImage(tempBitmap);

            return barcodeImage;
        }

        public static void DeleteBookToDB(Book book)
        {
            string sqlQuery = $"DELETE FROM Books WHERE Id = {book.Id} ";
                //$"DELETE FROM Book_Tags WHERE IdBook = {book.Id}";

            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                MessageBox.Show("Произошла ошибка соединения с БД. Перезапустите программу.", "Ошибка соединения с БД");
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            _ = command.ExecuteReader();

            //Закрываем соединение.
            connectToSql.Close();
        }

        public static void EditBookToDB(Book book)
        {
            string sqlQuery = $"UPDATE Books SET " +
                $"Name = '{book.Name.Trim()}'," +
                $"Genre = '{book.Genre.Trim()}'," +
                $"MainAuthor = '{book.MainAuthor.Trim()}'," +
                $"OtherAuthors = '{book.OtherAuthors.Trim()}'," +
                $"PublicationYear = {book.PublicationYear}," +
                $"Publisher = '{book.Publisher.Trim()}'," +
                $"ISBN = '{book.ISBN.Trim()}'," +
                $"NumberOfPages = {book.NumberOfPages}," +
                $"Description = '{book.Description.Trim()}'," +
                $"NumberOfBooks = {book.NumberOfBooks}," +
                $"InventoryNumber = '{book.InventoryNumber.Trim()}'," +
                $"BBK = '{book.BBK.Trim()}'," +
                $"IsObligatoryCopy = '{book.IsObligatoryCopy.Trim()}'," +
                $"Grif = '{book.Grif.Trim()}'," +
                $"CopyrightMark = '{book.CopyrightMark.Trim()}'," +
                $"Editor = '{book.Editor.Trim()}'," +
                $"Compiler = '{book.Compiler.Trim()}'" +
                $"WHERE id = {book.Id}" +
                $" ";

            sqlQuery += $"DELETE FROM Book_Tags WHERE ISBN = '{book.ISBN}'" +
                $" ";
            sqlQuery += $"UPDATE Books SET Tags = '' WHERE ISBN = '{book.ISBN}'" +
                $" ";

            if (!String.IsNullOrWhiteSpace(book.Tags))
            {
                book.Tags = book.Tags.Trim();
                if (book.Tags.EndsWith(";")
                    || book.Tags.Trim().EndsWith(",")
                    || book.Tags.Trim().EndsWith(".")
                    || book.Tags.Trim().EndsWith("'")
                    || book.Tags.Trim().EndsWith(":"))
                {
                    book.Tags = book.Tags.Remove(book.Tags.Length - 1);
                }

                string[] separators = { ",", ".", ";", ":", "'" };
                string[] tags = book.Tags.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                foreach (string tag in tags)
                {
                    sqlQuery += $"INSERT INTO Book_Tags (IdBook, Tag, ISBN)" +
                        $"SELECT {book.Id} As IdBook, '{tag.Trim()}' As Tag, '{book.ISBN.Trim()}' As ISBN" +
                        $" ";
                }
            }

            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                MessageBox.Show("Произошла ошибка соединения с БД. Перезапустите программу.", "Ошибка соединения с БД");
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            _ = command.ExecuteReader();

            //Закрываем соединение.
            connectToSql.Close();
        }

        public static void AddNewBookToDB(Book book)
        {
            string sqlQuery = $"INSERT INTO Books (Name, Genre, MainAuthor, OtherAuthors, PublicationYear, Publisher, ISBN, NumberOfPages, Description, NumberOfBooks, InventoryNumber, BBK, IsObligatoryCopy, Grif, CopyrightMark, Editor, Compiler)" +
                $"VALUES('{book.Name.Trim()}'" +
                $", '{book.Genre.Trim()}'" +
                $", '{book.MainAuthor.Trim()}'" +
                $", '{book.OtherAuthors.Trim()}'" +
                $", {book.PublicationYear}" +
                $", '{book.Publisher.Trim()}'" +
                $", '{book.ISBN.Trim()}'" +
                $", {book.NumberOfPages}" +
                $", '{book.Description.Trim()}'" +
                $",{book.NumberOfBooks}" +
                $", '{book.InventoryNumber.Trim()}'" +
                $", '{book.BBK.Trim()}'" +
                $", '{book.IsObligatoryCopy.Trim()}'" +
                $", '{book.Grif.Trim()}'" +
                $", '{book.CopyrightMark.Trim()}'" +
                $", '{book.Editor.Trim()}'" +
                $", '{book.Compiler.Trim()}')" +
                $" ";

            if (!String.IsNullOrWhiteSpace(book.Tags))
            {
                book.Tags = book.Tags.Trim();
                if (book.Tags.EndsWith(";")
                    || book.Tags.Trim().EndsWith(",")
                    || book.Tags.Trim().EndsWith(".")
                    || book.Tags.Trim().EndsWith("'")
                    || book.Tags.Trim().EndsWith(":"))
                {
                    book.Tags = book.Tags.Remove(book.Tags.Length - 1);
                }
                    
                string[] separators = { ",", ".", ";", ":", "'" };
                string[] tags = book.Tags.Split(separators, StringSplitOptions.RemoveEmptyEntries);

                foreach (string tag in tags)
                {
                    sqlQuery += $"INSERT INTO Book_Tags (IdBook, Tag, ISBN)" +
                        $"SELECT(Select TOP 1 Id From Books order by Id desc) As IdBook, '{tag.Trim()}' As Tag, '{book.ISBN.Trim()}' As ISBN" +
                        $" ";
                }
            }

            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                MessageBox.Show("Произошла ошибка соединения с БД. Перезапустите программу.", "Ошибка соединения с БД");
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            _ = command.ExecuteReader();

            //Закрываем соединение.
            connectToSql.Close();
        }

        public static ObservableCollection<Book> GetBooksFromDB()
        {
            ObservableCollection<Book> collection = new ObservableCollection<Book>();

            string sqlQuery = $"SELECT * FROM Books";
            //Строка подключения берется из конфиг. файла
            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                return null;
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            SqlDataReader booksFromDb = command.ExecuteReader();

            while (booksFromDb.Read())
            {
                collection.Add(new Book
                {
                    Id = Convert.ToInt32(booksFromDb["Id"]),
                    Name = booksFromDb["Name"].ToString(),
                    Genre = booksFromDb["Genre"].ToString(),
                    MainAuthor = booksFromDb["MainAuthor"].ToString(),
                    OtherAuthors = booksFromDb["OtherAuthors"].ToString(),
                    PublicationYear = Convert.ToInt32(booksFromDb["PublicationYear"]),
                    Publisher = booksFromDb["Publisher"].ToString(),
                    ISBN = booksFromDb["ISBN"].ToString(),
                    NumberOfPages = Convert.ToInt32(booksFromDb["NumberOfPages"]),
                    Description = booksFromDb["Description"].ToString(),
                    Barcode = booksFromDb["Barcode"].ToString(),
                    Tags = booksFromDb["Tags"].ToString(),
                    NumberOfBooks = Convert.ToInt32(booksFromDb["NumberOfBooks"]),
                    InventoryNumber = booksFromDb["InventoryNumber"].ToString(),
                    BBK = booksFromDb["BBK"].ToString(),
                    NumberOfAvailableBooks = Convert.ToInt32(booksFromDb["NumberOfAvailableBooks"]),
                    IsObligatoryCopy = booksFromDb["IsObligatoryCopy"].ToString(),
                    LocationOfBookID = Convert.ToInt32(booksFromDb["LocationOfBookID"]),
                    LocationOfBookName = booksFromDb["LocationOfBookName"].ToString(),
                    Grif = booksFromDb["Grif"].ToString(),
                    CopyrightMark = booksFromDb["CopyrightMark"].ToString(),
                    Editor = booksFromDb["Editor"].ToString(),
                    Compiler = booksFromDb["Compiler"].ToString()
                });
            }

            //Закрываем соединение.
            connectToSql.Close();
            //Соединение с БД успешно.
            return collection;
        }

        public static Book GetOneBookFromDB(string barcode)
        {
           Book book = new Book();

            string sqlQuery = $"SELECT * FROM Books WHERE Barcode like '%{barcode}%'";
            //Строка подключения берется из конфиг. файла
            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                return null;
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            SqlDataReader booksFromDb = command.ExecuteReader();

            while (booksFromDb.Read())
            {
                book = new Book
                {
                    Id = Convert.ToInt32(booksFromDb["Id"]),
                    Name = booksFromDb["Name"].ToString(),
                    Genre = booksFromDb["Genre"].ToString(),
                    MainAuthor = booksFromDb["MainAuthor"].ToString(),
                    OtherAuthors = booksFromDb["OtherAuthors"].ToString(),
                    PublicationYear = Convert.ToInt32(booksFromDb["PublicationYear"]),
                    Publisher = booksFromDb["Publisher"].ToString(),
                    ISBN = booksFromDb["ISBN"].ToString(),
                    NumberOfPages = Convert.ToInt32(booksFromDb["NumberOfPages"]),
                    Description = booksFromDb["Description"].ToString(),
                    Barcode = booksFromDb["Barcode"].ToString(),
                    Tags = booksFromDb["Tags"].ToString(),
                    NumberOfBooks = Convert.ToInt32(booksFromDb["NumberOfBooks"]),
                    InventoryNumber = booksFromDb["InventoryNumber"].ToString(),
                    BBK = booksFromDb["BBK"].ToString(),
                    NumberOfAvailableBooks = Convert.ToInt32(booksFromDb["NumberOfAvailableBooks"]),
                    IsObligatoryCopy = booksFromDb["IsObligatoryCopy"].ToString(),
                    LocationOfBookID = Convert.ToInt32(booksFromDb["LocationOfBookID"]),
                    LocationOfBookName = booksFromDb["LocationOfBookName"].ToString(),
                    Grif = booksFromDb["Grif"].ToString(),
                    CopyrightMark = booksFromDb["CopyrightMark"].ToString(),
                    Editor = booksFromDb["Editor"].ToString(),
                    Compiler = booksFromDb["Compiler"].ToString()
                };
            }

            //Закрываем соединение.
            connectToSql.Close();
            //Соединение с БД успешно.
            return book;
        }

        public static int IsBookAvailable(Book book)
        {
            int result = -1;

            string sqlQuery = $"SELECT LocationOfBookID FROM Books WHERE Id = {book.Id}";
            //Строка подключения берется из конфиг. файла
            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                MessageBox.Show("Произошла ошибка при подключении к Базе Данных.");
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            SqlDataReader booksFromDb = command.ExecuteReader();

            while (booksFromDb.Read())
            {
                result = Convert.ToInt32(booksFromDb["LocationOfBookID"]);
            }

            //Закрываем соединение.
            connectToSql.Close();
            //Соединение с БД успешно.
            return result;
        }
    }
}

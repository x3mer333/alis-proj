﻿using System;

namespace ALIS_Proj.Model.Entities
{
    public class Pasport
    {
        /// <summary>
        /// Номер паспорта
        /// </summary>
        public string Number { get; set; } = String.Empty;

        /// <summary>
        /// Серия паспорта
        /// </summary>
        public string Serial { get; set; } = String.Empty;

        /// <summary>
        /// Кем выдан паспорт
        /// </summary>
        public string IssuedBy { get; set; } = String.Empty;

        /// <summary>
        /// Дата выдачи
        /// </summary>
        public string IssueDate { get; set; } = String.Empty;

        public override string ToString()
        {
            return $"Номер:{Number} Серия:{Serial} Выдано:{IssuedBy} От:{IssueDate}";
        }
    }
}
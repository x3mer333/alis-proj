﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ALIS_Proj.Model.Entities
{
    public class Book: ICloneable
    {
        /// <summary>
        /// Идентификационный номер
        /// </summary>
        public int Id { get; set; } 

        /// <summary>
        /// Название книги
        /// </summary>
        public string Name { get; set; } = String.Empty;

        /// <summary>
        /// Жанровая категория
        /// </summary>
        public string Genre { get; set; } = String.Empty;

        /// <summary>
        /// Основной автор
        /// </summary>
        public string MainAuthor { get; set; } = String.Empty;

        /// <summary>
        /// Соавторы
        /// </summary>
        public string OtherAuthors { get; set; } = String.Empty;

        /// <summary>
        /// Год издания
        /// </summary>
        public int PublicationYear { get; set; } = 0;

        /// <summary>
        /// Наименование издательства
        /// </summary>
        public string Publisher { get; set; } = String.Empty;

        /// <summary>
        /// ISBN
        /// </summary>
        public string ISBN { get; set; } = String.Empty;

        /// <summary>
        /// Количество страниц
        /// </summary>
        public int NumberOfPages { get; set; } = 0;

        /// <summary>
        /// Описание книги
        /// </summary>
        public string Description { get; set; } = String.Empty;

        /// <summary>
        /// Штрихкод книги
        /// </summary>
        public string Barcode { get; set; } = String.Empty;

        /// <summary>
        /// Теги для поиска книги
        /// </summary>
        public string Tags { get; set; } = String.Empty;

        /// <summary>
        /// Количество экземпляров книги
        /// </summary>
        public int NumberOfBooks { get; set; } = 1;

        /// <summary>
        /// Инвентарный номер книги
        /// </summary>
        public string InventoryNumber { get; set; } = String.Empty;

        /// <summary>
        /// ББК книги
        /// </summary>
        public string BBK { get; set; } = String.Empty;

        /// <summary>
        /// Количество доступных экземпляров книги
        /// </summary>
        public int NumberOfAvailableBooks { get; set; } = 0;

        /// <summary>
        /// Обязательный экземпляр книги (нельзя отдавать)
        /// </summary>
        public string IsObligatoryCopy { get; set; } = String.Empty;

        /// <summary>
        /// ID владельца книги на данный момент
        /// </summary>
        public int LocationOfBookID { get; set; } = -1;

        /// <summary>
        /// ФИО владельца книги на данный момент
        /// </summary>
        public string LocationOfBookName { get; set; } = String.Empty;

        /// <summary>
        /// Гриф
        /// </summary>
        public string Grif { get; set; } = String.Empty;

        /// <summary>
        /// Авторский знак
        /// </summary>
        public string CopyrightMark { get; set; } = String.Empty;

        /// <summary>
        /// Редактор
        /// </summary>
        public string Editor { get; set; } = String.Empty;

        /// <summary>
        /// Составитель
        /// </summary>
        public string Compiler { get; set; } = String.Empty;

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}

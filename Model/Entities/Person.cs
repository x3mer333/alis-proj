﻿using System;
using System.ComponentModel;
using ALIS_Proj.Model.Enums;

namespace ALIS_Proj.Model.Entities
{
    /// <summary>
    /// Класс описывает характеристики клиентов приложения (пользователи и персонал)
    /// </summary>
    public class Person: ICloneable
    {
        /// <summary>
        /// Паспортные данные
        /// </summary>
        public Pasport PasportData { get; set; }

        public Person()
        {
            PasportData = new Pasport();
        }

        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; } = String.Empty;

        /// <summary>
        /// Фамилия
        /// </summary>
        public string Surname { get; set; } = String.Empty;

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronymic { get; set; } = String.Empty;

        /// <summary>
        /// ФИО
        /// </summary>
        public string FullName => $"{Surname} {Name} {Patronymic}";

        /// <summary>
        /// Уровень доступа (1 - админ, 2 - клиент)
        /// </summary>
        public AccessRole AccessRole { get; set; } = 0;

        /// <summary>
        /// Дата заведения пользователя в базу
        /// </summary>
        public DateTime CreateDate { get; set; } = DateTime.MinValue;

        /// <summary>
        /// День рождения
        /// </summary>
        public DateTime Birthday { get; set; } = DateTime.Today.AddYears(-18);

        /// <summary>
        /// Контактный номер
        /// </summary>
        public string PhoneNumber { get; set; } = String.Empty;

        /// <summary>
        /// Запасной контактный номер
        /// </summary>
        public string AltPhoneNumber { get; set; } = String.Empty;

        /// <summary>
        /// Домашний адрес
        /// </summary>
        public string HomeAddress { get; set; } = String.Empty;

        /// <summary>
        /// Номер школьной группы (если клиент учится в школе/колледже)
        /// </summary>
        public string SchoolGroupNumber { get; set; } = String.Empty;

        /// <summary>
        /// Пароль админа (в рамках данного приложения игнорируем правило безопасности - храним пароль в переменной)
        /// </summary>
        public string Password { get; set; } = String.Empty;

        /// <summary>
        /// Штрих код
        /// </summary>
        public string Barcode { get; set; } = String.Empty;

        public object Clone()
        {
            Pasport PasportDataTemp = new Pasport { 
                IssueDate = this.PasportData.IssueDate,
                IssuedBy = this.PasportData.IssuedBy,
                Number = this.PasportData.Number,
                Serial = this.PasportData.Serial};
            return new Person { 
                AccessRole = this.AccessRole,
                AltPhoneNumber = this.AltPhoneNumber,
                HomeAddress = this.HomeAddress,
                CreateDate = this.CreateDate,
                PasportData = PasportDataTemp,
                Barcode = this.Barcode,
                Birthday = this.Birthday,
                Id = this.Id,
                Name = this.Name,
                Password = this.Password,
                SchoolGroupNumber = this.SchoolGroupNumber,
                Patronymic = this.Patronymic,
                PhoneNumber = this.PhoneNumber,
                Surname = this.Surname
            };
        }

        /// <summary>
        /// Переопределяем ToString для возврата ФИО
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return FullName;
        }

        /// <summary>
        /// Проверка отсутствия имени, фамилии или отчества
        /// </summary>
        /// <returns></returns>
        public bool NamesIsEmpty ()
        {
            return (String.IsNullOrWhiteSpace(Name)
                || String.IsNullOrWhiteSpace(Surname)
                || String.IsNullOrWhiteSpace(Patronymic));
        }
    }
}
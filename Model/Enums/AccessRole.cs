﻿namespace ALIS_Proj.Model.Enums
{
    public enum AccessRole
    {
        Client = 0,
        Admin = 1,
        All = 2
    }
}

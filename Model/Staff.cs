﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using ALIS_Proj.Model.Entities;
using ALIS_Proj.Model.Enums;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Drawing2D;
using ALIS_Proj.Properties;
using System.IO;
using System.Drawing.Imaging;

namespace ALIS_Proj.Model
{
    /// <summary>
    /// Вспомогательный класс для обработки пользователей
    /// </summary>
    public static class Staff
    {
        /// <summary>
        /// Конвертация Bitmap в формат BitmapImage
        /// </summary>
        /// <param name="bmp">Исходное изображение в формате Bitmap</param>
        /// <returns></returns>
        private static BitmapImage ConverBitmapToBitmapImage(System.Drawing.Bitmap bmp)
        {
            MemoryStream stream = new MemoryStream();
            bmp.Save(stream, ImageFormat.Png);

            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.BeginInit();
            bitmapImage.StreamSource = stream;
            bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
            bitmapImage.EndInit();
            return bitmapImage;
        }

        /// <summary>
        /// Генерирование BitmapImage штрихкода на основе строки
        /// </summary>
        /// <param name="barcodeString">Строка штрихкода</param>
        /// <returns></returns>
        public static BitmapImage GetBarcodeImage(Person person)
        {
            //Настраиваем объект штрихкода (BarcodeLib)
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            b.BackColor = System.Drawing.Color.White;
            b.ForeColor = System.Drawing.Color.Black;
            b.IncludeLabel = false;
            b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
            b.Alignment = BarcodeLib.AlignmentPositions.LEFT;
            b.ImageFormat = System.Drawing.Imaging.ImageFormat.Png;
            System.Drawing.Font font = new System.Drawing.Font("verdana", 10f);
            b.LabelFont = font;
            b.Width = 300;//146;
            b.Height = 50;

            //Загружаем фон картинки
            Bitmap tempBitmap = new Bitmap(Resources.PersonBarcodeFrame);//(new BarcodeLib.Barcode().Encode(BarcodeLib.TYPE.CODE128B, barcodeString));
            //Создаем картинку штрихкода
            Bitmap tempBitmap2 = new Bitmap(b.Encode(BarcodeLib.TYPE.CODE128B, person.Barcode));
            //Создаем графическое полотно для рисования и объеденения изображений
            Graphics graphicImage = Graphics.FromImage(tempBitmap);

            //Задаем отрисовку со сглаживанием
            graphicImage.SmoothingMode = SmoothingMode.AntiAlias;

            //Объединяем фон и штрихкод
            graphicImage.DrawImage(tempBitmap2, new Rectangle(new System.Drawing.Point(45, 155), new System.Drawing.Size(tempBitmap2.Width-50, tempBitmap2.Height)),
                new Rectangle(new System.Drawing.Point(0, 0), new System.Drawing.Size(tempBitmap2.Width-50, tempBitmap2.Height)), GraphicsUnit.Pixel);

            //Добавляем надписи - сведения о читателе
            graphicImage.DrawString(person.Surname, new Font("Arial", 11, System.Drawing.FontStyle.Bold), SystemBrushes.WindowText, new System.Drawing.Point(60, 72));
            graphicImage.DrawString(person.Name, new Font("Arial", 11, System.Drawing.FontStyle.Bold), SystemBrushes.WindowText, new System.Drawing.Point(60, 92));
            graphicImage.DrawString(person.Patronymic, new Font("Arial", 11, System.Drawing.FontStyle.Bold), SystemBrushes.WindowText, new System.Drawing.Point(60, 112));
            graphicImage.DrawString(person.SchoolGroupNumber, new Font("Arial", 11, System.Drawing.FontStyle.Bold), SystemBrushes.WindowText, new System.Drawing.Point(115, 132));
            
            graphicImage.DrawString(person.Barcode + "                                      г. Северск", new Font("Arial", 9, System.Drawing.FontStyle.Bold), SystemBrushes.WindowText, new System.Drawing.Point(45, 205));

            //Конвертируем в требуемый формат BitmapImage
            BitmapImage barcodeImage = ConverBitmapToBitmapImage(tempBitmap);

            return barcodeImage;
        }

        public static void DeletePersonDB(Person person)
        {
            string sqlQuery = $"DELETE FROM Personnel WHERE Id = {person.Id} ";

            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                MessageBox.Show("Произошла ошибка соединения с БД. Перезапустите программу.", "Ошибка соединения с БД");
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            _ = command.ExecuteReader();

            //Закрываем соединение.
            connectToSql.Close();
        }

        public static void UpdateNewPersonToDB(Person person)
        {
            string sqlQuery = $"SET DATEFORMAT dmy;\n" +
                $"UPDATE Personnel SET " +
                $"Name = '{person.Name.Trim()}'" +
                $", Surname = '{person.Surname.Trim()}'" +
                $", Patronymic = '{person.Patronymic.Trim()}'" +
                $", AccessRole = {Convert.ToInt32(person.AccessRole)}" +
                $", Birthday = '{person.Birthday.ToString()}'" +
                $", PhoneNumber = '{person.PhoneNumber.Trim()}'" +
                $", AltPhoneNumber = '{person.AltPhoneNumber.Trim()}'" +
                $", HomeAddress = '{person.HomeAddress.Trim()}'" +
                $", SchoolGroupNumber = '{person.SchoolGroupNumber.Trim()}'" +
                $", Pasportnumber = '{person.PasportData.Number.Trim()}'" +
                $", Pasportserial = '{person.PasportData.Serial.Trim()}'" +
                $", Pasportfrom = '{person.PasportData.IssuedBy.Trim()}'" +
                $", PasportIssueDate = '{person.PasportData.IssueDate.Trim()}'" +
                $"WHERE id = {person.Id}";

            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                MessageBox.Show("Произошла ошибка соединения с БД. Перезапустите программу.", "Ошибка соединения с БД");
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            _ = command.ExecuteReader();

            //Закрываем соединение.
            connectToSql.Close();
        }

        public static void AddNewPersonToDB(Person person)
        {
            string sqlQuery = $"SET DATEFORMAT dmy;\n" +
                "INSERT INTO Personnel (Name, Surname, Patronymic, AccessRole, Birthday, PhoneNumber, AltPhoneNumber, HomeAddress, SchoolGroupNumber, Password, Pasportnumber, Pasportserial, Pasportfrom, PasportIssueDate)" +
                $"VALUES('{person.Name.Trim()}'" +
                $", '{person.Surname.Trim()}'" +
                $", '{person.Patronymic.Trim()}'" +
                $",  {Convert.ToInt32(person.AccessRole)}" +
                $", '{person.Birthday.ToString()}'" +
                $", '{person.PhoneNumber.Trim()}'" +
                $", '{person.AltPhoneNumber.Trim()}'" +
                $", '{person.HomeAddress.Trim()}'" +
                $", '{person.SchoolGroupNumber.Trim()}'" +
                $", '{person.Password.Trim()}'" +
                $", '{person.PasportData.Number.Trim()}'" +
                $", '{person.PasportData.Serial.Trim()}'" +
                $", '{person.PasportData.IssuedBy.Trim()}'" +
                $", '{person.PasportData.IssueDate.Trim()}')" +
                $" ";

            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Сообщаем об ошибке соединения с БД
                MessageBox.Show("Произошла ошибка соединения с БД. Перезапустите программу.", "Ошибка соединения с БД");
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            _ = command.ExecuteReader();

            //Закрываем соединение.
            connectToSql.Close();
        }

        /// <summary>
        /// Извлекаем из БД всех пользователей, имеющих право на пользование приложения (для авторизации).
        /// </summary>
        /// <param name="collection">Коллекция для наполнения объектами Person</param>
        /// <param name="accessRole">Фильтр по уровню доступа Person</param>
        /// <returns>True - если при соединении с БД возникла ошибка</returns>
        public static ObservableCollection<Person> GetPersonsFromDB(AccessRole accessRole)
        {
            ObservableCollection<Person> collection = new ObservableCollection<Person>();

            string sqlQuery;

            if (accessRole == AccessRole.All)
            {
                sqlQuery = $"select * from personnel";
            }
            else
            {
                sqlQuery = $"select * from personnel where accessrole in ({Convert.ToInt32(accessRole)})";
            }
            
            //Строка подключения берется из конфиг. файла
            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Как признак ошибки подключения
                return null;
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            SqlDataReader personnelFromDb = command.ExecuteReader();

            while (personnelFromDb.Read())
            {
                collection.Add(new Person
                {
                    Id = Convert.ToInt32(personnelFromDb["id"]),
                    Name = personnelFromDb["name"].ToString(),
                    Surname = personnelFromDb["surname"].ToString(),
                    Patronymic = personnelFromDb["patronymic"].ToString(),
                    AccessRole = (AccessRole) Convert.ToInt32(personnelFromDb["accessrole"]),
                    CreateDate = Convert.ToDateTime(personnelFromDb["createdate"]),
                    Birthday = Convert.ToDateTime(personnelFromDb["birthday"]),
                    PhoneNumber = personnelFromDb["phonenumber"].ToString(),
                    AltPhoneNumber = personnelFromDb["altphonenumber"].ToString(),
                    HomeAddress = personnelFromDb["homeaddress"].ToString(),
                    SchoolGroupNumber = personnelFromDb["SchoolGroupNumber"].ToString(),
                    Password = personnelFromDb["Password"].ToString(),
                    PasportData = new Pasport
                    {
                        Number = personnelFromDb["pasportnumber"].ToString(),
                        Serial = personnelFromDb["pasportserial"].ToString(),
                        IssuedBy = personnelFromDb["pasportfrom"].ToString(),
                        IssueDate = personnelFromDb["PasportIssueDate"].ToString()
                    },
                    Barcode = personnelFromDb["barcode"].ToString()
                });
            }

            //Закрываем соединение.
            connectToSql.Close();
            //Соединение с БД успешно.
            return collection;
        }

        /// <summary>
        /// Извлекаем из БД одного пользователя по штрихкоду.
        /// </summary>
        /// <param name="collection">Коллекция для наполнения объектами Person</param>
        /// <param name="accessRole">Фильтр по уровню доступа Person</param>
        /// <returns>True - если при соединении с БД возникла ошибка</returns>
        public static Person GetOnePersonByBarcode(string barcode)
        {
            Person person = new Person();

            string sqlQuery = $"select * from personnel where barcode like '%{barcode}%'";

            //Строка подключения берется из конфиг. файла
            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Как признак ошибки подключения
                return null;
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            SqlDataReader personnelFromDb = command.ExecuteReader();

            while (personnelFromDb.Read())
            {
                person = new Person
                {
                    Id = Convert.ToInt32(personnelFromDb["id"]),
                    Name = personnelFromDb["name"].ToString(),
                    Surname = personnelFromDb["surname"].ToString(),
                    Patronymic = personnelFromDb["patronymic"].ToString(),
                    AccessRole = (AccessRole)Convert.ToInt32(personnelFromDb["accessrole"]),
                    CreateDate = Convert.ToDateTime(personnelFromDb["createdate"]),
                    Birthday = Convert.ToDateTime(personnelFromDb["birthday"]),
                    PhoneNumber = personnelFromDb["phonenumber"].ToString(),
                    AltPhoneNumber = personnelFromDb["altphonenumber"].ToString(),
                    HomeAddress = personnelFromDb["homeaddress"].ToString(),
                    SchoolGroupNumber = personnelFromDb["SchoolGroupNumber"].ToString(),
                    Password = personnelFromDb["Password"].ToString(),
                    PasportData = new Pasport
                    {
                        Number = personnelFromDb["pasportnumber"].ToString(),
                        Serial = personnelFromDb["pasportserial"].ToString(),
                        IssuedBy = personnelFromDb["pasportfrom"].ToString(),
                        IssueDate = personnelFromDb["PasportIssueDate"].ToString()
                    },
                    Barcode = personnelFromDb["barcode"].ToString()
                };
            }

            //Закрываем соединение.
            connectToSql.Close();
            //Соединение с БД успешно.
            return person;
        }

        /// <summary>
        /// Извлекаем из БД одного пользователя по ID.
        /// </summary>
        /// <param name="collection">Коллекция для наполнения объектами Person</param>
        /// <param name="accessRole">Фильтр по уровню доступа Person</param>
        /// <returns>True - если при соединении с БД возникла ошибка</returns>
        public static Person GetOnePersonById(int id)
        {
            Person person = new Person();

            string sqlQuery = $"select * from personnel where Id = {id} ";

            //Строка подключения берется из конфиг. файла
            var connectToSql = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectToDB"].ConnectionString);
            try
            {
                //Открываем соединение
                connectToSql.Open();
            }
            catch (Exception)
            {
                //Как признак ошибки подключения
                return null;
            }

            SqlCommand command = new SqlCommand(sqlQuery, connectToSql);
            SqlDataReader personnelFromDb = command.ExecuteReader();

            while (personnelFromDb.Read())
            {
                person = new Person
                {
                    Id = Convert.ToInt32(personnelFromDb["id"]),
                    Name = personnelFromDb["name"].ToString(),
                    Surname = personnelFromDb["surname"].ToString(),
                    Patronymic = personnelFromDb["patronymic"].ToString(),
                    AccessRole = (AccessRole)Convert.ToInt32(personnelFromDb["accessrole"]),
                    CreateDate = Convert.ToDateTime(personnelFromDb["createdate"]),
                    Birthday = Convert.ToDateTime(personnelFromDb["birthday"]),
                    PhoneNumber = personnelFromDb["phonenumber"].ToString(),
                    AltPhoneNumber = personnelFromDb["altphonenumber"].ToString(),
                    HomeAddress = personnelFromDb["homeaddress"].ToString(),
                    SchoolGroupNumber = personnelFromDb["SchoolGroupNumber"].ToString(),
                    Password = personnelFromDb["Password"].ToString(),
                    PasportData = new Pasport
                    {
                        Number = personnelFromDb["pasportnumber"].ToString(),
                        Serial = personnelFromDb["pasportserial"].ToString(),
                        IssuedBy = personnelFromDb["pasportfrom"].ToString(),
                        IssueDate = personnelFromDb["PasportIssueDate"].ToString()
                    },
                    Barcode = personnelFromDb["barcode"].ToString()
                };
            }

            //Закрываем соединение.
            connectToSql.Close();
            //Соединение с БД успешно.
            return person;
        }
    }
}